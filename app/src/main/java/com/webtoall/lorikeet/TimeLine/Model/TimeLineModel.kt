package com.webtoall.lorikeet.TimeLine.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Vipul Asri on 05-12-2015.
 */
@Parcelize
class TimeLineModel(
    var message: String,
    var date: String,
    var status: OrderStatus,
    var position:Int
) : Parcelable