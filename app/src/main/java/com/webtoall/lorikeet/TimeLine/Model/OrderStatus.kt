package com.webtoall.lorikeet.TimeLine.Model

enum class OrderStatus {
    COMPLETED,
    ACTIVE,
    INACTIVE
}