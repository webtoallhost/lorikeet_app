package com.webtoall.lorikeet.TimeLine.example

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.vipulasri.timelineview.TimelineView
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.TimeLine.Model.OrderStatus
import com.webtoall.lorikeet.TimeLine.Model.TimeLineModel
import com.webtoall.lorikeet.TimeLine.extensions.formatDateTime
import com.webtoall.lorikeet.TimeLine.extensions.setGone
import com.webtoall.lorikeet.TimeLine.extensions.setVisible
import com.webtoall.lorikeet.TimeLine.utils.VectorDrawableUtils

class ExampleTimeLineAdapter(private val mFeedList: List<TimeLineModel>) : RecyclerView.Adapter<ExampleTimeLineAdapter.TimeLineViewHolder>() {

    private lateinit var mLayoutInflater: LayoutInflater

    override fun getItemViewType(position: Int): Int {
        return TimelineView.getTimeLineViewType(position, itemCount)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TimeLineViewHolder {

        if(!::mLayoutInflater.isInitialized) {
            mLayoutInflater = LayoutInflater.from(parent.context)
        }

        return TimeLineViewHolder(mLayoutInflater.inflate(R.layout.item_timeline, parent, false), viewType)
    }

    override fun onBindViewHolder(holder: TimeLineViewHolder, position: Int) {

        val timeLineModel = mFeedList[position]

        when {
            timeLineModel.status == OrderStatus.INACTIVE -> {
                setMarker1(holder, R.drawable.ic_marker, R.color.white,timeLineModel.position)
            }
            timeLineModel.status == OrderStatus.ACTIVE -> {
                setMarker(holder, R.drawable.choose_language, R.color.blue, timeLineModel.position)
            }
            else -> {
                setMarker(holder, R.drawable.choose_language,R.color.blue,timeLineModel.position)
            }
        }

        if (timeLineModel.date.isNotEmpty()) {
            holder.date.setVisible()
            holder.date.text = timeLineModel.date.formatDateTime("yyyy-MM-dd HH:mm", "MMM dd,yyyy - hh:mm a")//"hh:mm a, dd-MMM-yyyy"
        } else
            holder.date.setGone()

        holder.message.text = timeLineModel.message
    }

    private fun setMarker(
        holder: TimeLineViewHolder,
        drawableResId: Int,
        colorFilter: Int,
        position: Int
    ) {
        //holder.timeline.marker = VectorDrawableUtils.getDrawable(holder.itemView.context, drawableResId, ContextCompat.getColor(holder.itemView.context, colorFilter))
        holder.timeline.marker = VectorDrawableUtils.getDrawable(holder.itemView.context, drawableResId)
        if(position==1){
            holder.timeline.setEndLineColor(R.color.yellow, 1)
            holder.timeline.setStartLineColor(R.color.yellow, 1)
            //holder.timeline.initLine(1)
        }else if(position==4){
           /* holder.timeline.setEndLineColor(R.color.yellow, 2)
            holder.timeline.setStartLineColor(R.color.yellow, 2)*/
            holder.timeline.initLine(2)
        }else {
            holder.timeline.setEndLineColor(R.color.yellow, 10)
            holder.timeline.setStartLineColor(R.color.yellow, 10)
        }
    }
    private fun setMarker1(
        holder: TimeLineViewHolder,
        drawableResId: Int,
        colorFilter: Int,
        position: Int
    ) {
        holder.timeline.marker = VectorDrawableUtils.getDrawable(holder.itemView.context, drawableResId, ContextCompat.getColor(holder.itemView.context, colorFilter))
        holder.date.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.gray))
        holder.message.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.light_gray))
       /* if(position==4){
            holder.timeline.setEndLineColor(R.color.darkblue, 10)
            holder.timeline.setStartLineColor(R.color.darkblue, 2)
        }else {
            holder.timeline.setEndLineColor(R.color.darkblue, 10)
            holder.timeline.setStartLineColor(R.color.darkblue, 10)
        }*/
        //holder.timeline.marker = VectorDrawableUtils.getDrawable(holder.itemView.context, drawableResId)
    }

    override fun getItemCount() = mFeedList.size

    inner class TimeLineViewHolder(itemView: View, viewType: Int) : RecyclerView.ViewHolder(itemView) {

        val date = itemView.findViewById<TextView>(R.id.text_timeline_date)
        val message = itemView.findViewById<TextView>(R.id.text_timeline_title)
        val timeline = itemView.findViewById<TimelineView>(R.id.timeline)

        init {
            timeline.initLine(viewType)
        }
    }

}