package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R

class FiltersMainAdapter (
    private val context : Context,
    private val images: List<FilterMain>
) : RecyclerView.Adapter<FiltersMainAdapter.ImageViewHolder>() {
    private var row_index=-1
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = itemView.findViewById<ImageView>(R.id.icon)
        val title = itemView.findViewById<TextView>(R.id.sort_by)

        fun bindView(
            image: FilterMain,
            images: List<FilterMain>,
            position: Int
        ) {
            img.setImageResource(image.image)
            title.text= image.text
           /* itemView.setOnClickListener { v: View ->
                val position: Int = absoluteAdapterPosition
                for (i in images.indices)
                {
                    img.visibility=View.VISIBLE
                }

                Toast.makeText(itemView.context, "You clicked on color # ${position + 1}", Toast.LENGTH_SHORT).show()
            }*/
        }
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.custom_list, parent, false))

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position],images,position)
        holder.itemView.setOnClickListener(View.OnClickListener {
            row_index = position
            notifyDataSetChanged()
        })
        if (row_index == position) {
            holder.img.visibility=View.VISIBLE
            holder.title.setTextColor(ContextCompat.getColor(context,R.color.blue))
            Toast.makeText(this.context,"You clicked on color # ${position + 1}", Toast.LENGTH_SHORT).show()


            //holder.tv1.setTextColor(Color.parseColor("#ffffff"))
        } else {
            holder.img.visibility=View.INVISIBLE
            holder.title.setTextColor(ContextCompat.getColor(context,R.color.white))
        }
    }
}

class FilterMain (val image : Int, val text : String)