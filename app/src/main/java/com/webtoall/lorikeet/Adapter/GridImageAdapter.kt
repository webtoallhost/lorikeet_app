package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Activity.Products
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.DataModel.item

class GridImageAdapter (
    private val context : Context,
    private val itemList: List<item>
) : RecyclerView.Adapter<GridImageAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img : ImageView = view.findViewById(R.id.item_image_shapeableIv)
        val title: TextView = view.findViewById(R.id.item_dressname_tv)
        val title1: TextView = view.findViewById(R.id.item_price_tv)


        fun bindView(i: item, context: Context)         {
            img.setImageResource(i.image)
            title.text = i.dressname
            title1.text = i.price
            itemView.setOnClickListener {
                context.startActivity(Intent(context, Products::class.java))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.dress_item, parent, false))

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(itemList[position],context)
    }
}

data class category (val image: Int, val name : String)
/*
class GridImageAdapter (var context: Context, var arrayList : ArrayList<item> ): BaseAdapter() {
    override fun getCount(): Int {
        return arrayList.size    }

    override fun getItem(p0: Int): Any {
        return arrayList.get(p0)
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        val view : View = View.inflate(context , R.layout.item, null)
        val img : ImageView = view.findViewById(R.id.item_image_shapeableIv)
        val title: TextView = view.findViewById(R.id.item_dressname_tv)
        val title1: TextView = view.findViewById(R.id.item_price_tv)


        val listItem: item = arrayList.get(p0)

        img.setImageResource(listItem.image)
        title.text = listItem.dressname
        title1.text = listItem.price
        return view
    }

}*/
