package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.DataModel.SelectStyleDataModel
import com.webtoall.lorikeet.R

class HandCollarAdapter (
        private val context : Context,
        private val images: List<SelectStyleDataModel>
) : RecyclerView.Adapter<HandCollarAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val dressImage = itemView.findViewById<ImageView>(R.id.collar_type_item_collar_imageView)
        private val selectedImageView = itemView.findViewById<ImageView>(R.id.collar_type_item_selected_item_imageView)
        private val title = itemView.findViewById<TextView>(R.id.collar_type_item_collar_title_textView)

        fun bindView(item: SelectStyleDataModel)         {
            dressImage.setImageResource(item.image)
            title.text=item.title
            if(item.selected)
                selectedImageView.visibility= View.VISIBLE
            else
                selectedImageView.visibility= View.GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
            LayoutInflater.from(context).inflate(R.layout.collar_type_item, parent, false))

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position])
    }
}