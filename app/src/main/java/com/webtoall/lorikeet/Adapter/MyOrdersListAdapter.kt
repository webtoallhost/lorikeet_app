package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Activity.MyOrdersDetails
import com.webtoall.lorikeet.DataModel.MyOrdersDataModel
import com.webtoall.lorikeet.R

class MyOrdersListAdapter(
        private val context: Context,
        private val itemList: List<MyOrdersDataModel>
) : RecyclerView.Adapter<MyOrdersListAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        /*val img : ImageView = view.findViewById(R.id.item_image_shapeableIv)
        val title: TextView = view.findViewById(R.id.item_dressname_tv)
        val title1: TextView = view.findViewById(R.id.item_price_tv)
        val favouritesImage: ImageView = view.findViewById(R.id.item_favourite_imageView)*/
        val status: TextView = view.findViewById(R.id.my_orders_list_item_status_textView)


        fun bindView(i: MyOrdersDataModel, context: Context)         {
           /* img.setImageResource(i.image)
            title.text = i.dressname
            title1.text = i.price
            favouritesImage.setImageResource(R.drawable.ic_favourite)*/

            when (i.statusId) {
                3 -> drawableImage(R.drawable.cancelled_background,context,R.color.white)
                2 -> drawableImage(R.drawable.completed_background, context, R.color.white)
                else -> drawableImage(R.drawable.on_going_background, context, R.color.black)
            }

            status.text = i.status

            itemView.setOnClickListener {
                val intent = Intent(context,MyOrdersDetails::class.java)
                when (i.statusId) {
                    3 -> {
                       Toast.makeText(context,"No Navigation",Toast.LENGTH_SHORT).show()}
                    2 -> {
                        intent.putExtra("Value","2")
                        context.startActivity(intent)}
                    else -> {
                        intent.putExtra("Value","1")
                        context.startActivity(intent)}
                }

            }
        }

        private fun drawableImage(drawableVal: Int, context: Context, colorVal: Int) {
            status.setTextColor(ContextCompat.getColor(context,colorVal))
            val sdk = android.os.Build.VERSION.SDK_INT
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                status.setBackgroundDrawable(ContextCompat.getDrawable(context, drawableVal))
            } else {
                status.background = ContextCompat.getDrawable(context, drawableVal);
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.my_orders_list_item, parent, false))

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(itemList[position],context)
    }
}