package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Activity.TailorMensFabric
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.DataModel.collections

class GridImageAdapterCollections (
    private val context : Context,
    private val itemList: List<collections>
) : RecyclerView.Adapter<GridImageAdapterCollections.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img : ImageView = view.findViewById(R.id.tailorCollection_dressimage)
        val title: TextView = view.findViewById(R.id.tailorCollection_text)


        fun bindView(i: collections, context: Context)         {
            img.setImageResource(i.image)
            title.text = i.name
            itemView.setOnClickListener {
                context.startActivity(Intent(context, TailorMensFabric::class.java))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.collections, parent, false))

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(itemList[position],context)
    }
}
