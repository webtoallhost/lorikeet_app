package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.DataModel.PaymentMethodDataModel
import com.webtoall.lorikeet.R

class PaymentMethodAdapter (
    private val context : Context,
    private val paymentMethod: List<PaymentMethodDataModel>
) : RecyclerView.Adapter<PaymentMethodAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val cardType = itemView.findViewById<ImageView>(R.id.payment_method_item_cardType_imageView)
        private val cardNumber = itemView.findViewById<TextView>(R.id.payment_method_item_cardNumber_textView)
        private val cardSelection = itemView.findViewById<ImageView>(R.id.payment_method_item_selectedcard_imageView)

        fun bindView(paymentMethod: PaymentMethodDataModel)         {
            cardType.setImageResource(paymentMethod.cardTypeImage)
            cardNumber.text= paymentMethod.cardNumber
            if(paymentMethod.selected) {
                cardSelection.visibility = View.VISIBLE
            }else{
                cardSelection.visibility =View.GONE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.payment_method_item, parent, false))

    override fun getItemCount(): Int = paymentMethod.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(paymentMethod[position])
    }
}