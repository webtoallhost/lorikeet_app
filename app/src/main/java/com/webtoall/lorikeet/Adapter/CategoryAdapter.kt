package com.webtoall.lorikeet

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.github.siyamed.shapeimageview.HexagonImageView
import com.webtoall.lorikeet.Activity.Products
import com.webtoall.lorikeet.Activity.TailorCollectionActivity

class CategoryAdapter (
        private val context : Context,
        private val images: List<category>
) : RecyclerView.Adapter<CategoryAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = itemView.findViewById<HexagonImageView>(R.id.category_image_shapeableIv)
        val title = itemView.findViewById<TextView>(R.id.category_name_tv)

        fun bindView(image: category,context: Context)         {
            img.setImageResource(image.image)
            title.text= image.name
            itemView.setOnClickListener {
                context.startActivity(Intent(context, TailorCollectionActivity::class.java))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
            LayoutInflater.from(context).inflate(R.layout.category, parent, false))

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position],context)
    }
}

data class category (val image: Int, val name : String)
