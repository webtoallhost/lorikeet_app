package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R


class TailorMensFabricAdapterColor(
        private val context: Context,
        private var images: List<Int>
) : RecyclerView.Adapter<TailorMensFabricAdapterColor.ViewHolder>() {
    private var row_index=-1
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val img = itemView.findViewById<ImageView>(R.id.imagecolor)
        val linearLayout=itemView.findViewById<LinearLayout>(R.id.shapes_linearLayout)


        init {
            itemView.setOnClickListener { v: View ->
                val position: Int = adapterPosition
                for (i in 0 until images.size)
                {
                    itemView.setBackground(ContextCompat.getDrawable(context,
                        R.drawable.round
                    ))
                }

                Toast.makeText(itemView.context, "You clicked on color # ${position + 1}", Toast.LENGTH_SHORT).show()
            }

        }
    }





    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.shaphes, parent, false)
        return ViewHolder(v)
    }



    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.img.setImageResource(images[position])
        //holder.tv1.setText(android_versionnames.get(position))

        holder.linearLayout.setOnClickListener(View.OnClickListener {
            row_index = position
            notifyDataSetChanged()
        })
        if (row_index === position) {
            holder.img.setBackground(ContextCompat.getDrawable(context,
                R.drawable.round
            ))
            Toast.makeText(this.context,"You clicked on color # ${position + 1}", Toast.LENGTH_SHORT).show()
            //holder.tv1.setTextColor(Color.parseColor("#ffffff"))
        } else {
            holder.img.setBackground(ContextCompat.getDrawable(context,
                R.drawable.unchecked_item_round
            ))
            //holder.tv1.setTextColor(Color.parseColor("#000000"))
        }
    }
}
