package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Activity.Products
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.DataModel.item

class FavouritesGridImageAdapter (
        private val context : Context,
        private val itemList: List<item>
) : RecyclerView.Adapter<FavouritesGridImageAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img : ImageView = view.findViewById(R.id.item_image_shapeableIv)
        val title: TextView = view.findViewById(R.id.item_dressname_tv)
        val title1: TextView = view.findViewById(R.id.item_price_tv)
        val favouritesImage:ImageView = view.findViewById(R.id.item_favourite_imageView)


        fun bindView(i: item, context: Context)         {
            img.setImageResource(i.image)
            title.text = i.dressname
            title1.text = i.price
            favouritesImage.setImageResource(R.drawable.ic_favourite)

            itemView.setOnClickListener {
                context.startActivity(Intent(context,Products::class.java))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item, parent, false))

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(itemList[position],context)
    }
}