package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R

class ShapesAdapter (
    private val context : Context,
    private val images: List<color1>
) : RecyclerView.Adapter<ShapesAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val img = itemView.findViewById<ImageView>(R.id.imagecolor)

        fun bindView(image: color1) {

            img.setImageResource(image.image)

        }
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder =
        ImageViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.shaphes, parent, false)
        )

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position])
    }
}
class color1 (val image: Int)
