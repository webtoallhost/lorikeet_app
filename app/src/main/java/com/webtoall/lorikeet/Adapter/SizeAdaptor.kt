package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R

class SizeAdapter (
    private val context : Context,
    private val images: List<size>
) : RecyclerView.Adapter<SizeAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val title = itemView.findViewById<TextView>(R.id.text)

        fun bindView(image: size) {

            title.text= image.sizename

        }
    }

    class size ( val sizename : String)



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder =
        ImageViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.products_size,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position])
    }
}