package com.webtoall.lorikeet

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Spinner
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CartAdapter (
        private val context : Context,
        private val images: List<cart>
) : RecyclerView.Adapter<CartAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = itemView.findViewById<ImageView>(R.id.cart_shapeableIv_image)
        val title = itemView.findViewById<TextView>(R.id.cart_productname_tv)
        val amt = itemView.findViewById<TextView>(R.id.cart_singleamount_tv)
        val spinner = itemView.findViewById<Spinner>(R.id.spinner)
        val tamt = itemView.findViewById<TextView>(R.id.cart_totalamount_tv)


        fun bindView(image: cart, context: Context)         {
            val adapter = ArrayAdapter.createFromResource(
                context,
                R.array.Languages,
                android.R.layout.simple_spinner_dropdown_item
            )

            img.setImageResource(image.pic)
            title.text= image.title
            //amt.text= image.singleamount
            tamt.text= image.totalamount

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner?.adapter = adapter
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
            LayoutInflater.from(context).inflate(R.layout.cart_cardview, parent, false))

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position],context)
    }
}

data class cart (val pic : Int, val title : String, val singleamount : String, val totalamount : String)

