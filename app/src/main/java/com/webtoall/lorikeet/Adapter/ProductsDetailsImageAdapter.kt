package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.DataModel.item
import com.webtoall.lorikeet.R

class ProductsDetailsImageAdapter (
    private val context : Context,
    private val images: List<item>
) : RecyclerView.Adapter<ProductsDetailsImageAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = itemView.findViewById<ImageView>(R.id.product_details_image_item_imageView)

        fun bindView(image: item) {
            img.setImageResource(image.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder =
        ImageViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.product_details_image_item, parent, false)
        )

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position])
    }
}