package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Activity.Products
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.DataModel.item

class StoresItemAdapter (
    private val context : Context,
    private val images: List<item>
) : RecyclerView.Adapter<StoresItemAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = itemView.findViewById<ImageView>(R.id.item_image_shapeableIv)
        val title = itemView.findViewById<TextView>(R.id.item_dressname_tv)
        val price = itemView.findViewById<TextView>(R.id.item_price_tv)

        fun bindView(image: item, context: Context) {
            img.setImageResource(image.image)
            title.text= image.dressname
            price.text= image.price
            itemView.setOnClickListener {
                context.startActivity(Intent(context, Products::class.java))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.item, parent, false))

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position],context)
    }
}