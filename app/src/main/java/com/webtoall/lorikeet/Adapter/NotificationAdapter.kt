package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R

class NotificationAdapter (
    private val context : Context,
    private val list_notification : List<notify>
) : RecyclerView.Adapter<NotificationAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = itemView.findViewById<ImageView>(R.id.sample3)
        val noti_detail = itemView.findViewById<TextView>(R.id.text_product_details)
        val noti_detail2 = itemView.findViewById<TextView>(R.id.time)


        fun bindView(image: notify)         {
            img.setImageResource(image.image)
            noti_detail.text= image.name
            noti_detail2.text= image.time
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.notification_list, parent, false))

    override fun getItemCount(): Int = list_notification.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(list_notification[position])
    }
}

data  class notify (val image : Int , val name: String, val time : String)