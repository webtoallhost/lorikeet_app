package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R

class UploadPhotoAdapter(
    private val context : Context,
    private val itemList: List<uploadPhoto>
) : RecyclerView.Adapter<UploadPhotoAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = itemView.findViewById<ImageView>(R.id.remove_upload_list)

        fun bindView(image: uploadPhoto)         {
            img.setImageResource(image.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.upload_photo_item, parent, false))

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(itemList[position])
    }
}

class uploadPhoto (val image: Int)