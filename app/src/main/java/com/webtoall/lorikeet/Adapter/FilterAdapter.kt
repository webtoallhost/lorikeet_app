package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R

class FilterAdapter (
    private val context : Context,
    private val images: List<filter>
    ) : RecyclerView.Adapter<FilterAdapter.ImageViewHolder>() {
    var row_index=-1
        class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val img = itemView.findViewById<ImageView>(R.id.icon)
            val title = itemView.findViewById<TextView>(R.id.sort_by)

            fun bindView(image: filter) {
                img.setImageResource(image.image)
                title.text= image.text
            }
        }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
                ImageViewHolder = ImageViewHolder(
            LayoutInflater.from(context).inflate(R.layout.custom_list, parent, false))

        override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position])
        holder.itemView.setOnClickListener(View.OnClickListener {
            row_index = position
            notifyDataSetChanged()
        })
        if (row_index == position) {
            holder.img.visibility=View.VISIBLE
            holder.title.setTextColor(ContextCompat.getColor(context,R.color.blue))
            Toast.makeText(this.context,"You clicked on color # ${position + 1}", Toast.LENGTH_SHORT).show()


            //holder.tv1.setTextColor(Color.parseColor("#ffffff"))
        } else {
            holder.img.visibility=View.INVISIBLE
            holder.title.setTextColor(ContextCompat.getColor(context,R.color.white))
        }

    }
}

class filter (val image : Int, val text : String)