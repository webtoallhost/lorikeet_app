package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R

class StoresAdapter (
    private val context : Context,
    private val images: List<stores>
) : RecyclerView.Adapter<StoresAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val logo = itemView.findViewById<ImageView>(R.id.tailor_stores_logo)
        val name = itemView.findViewById<TextView>(R.id.tailor_stores_name)

        fun bindView(image: stores)         {
            logo.setImageResource(image.logo)
            name.text= image.name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.stores, parent, false))

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position])
    }
}

data class stores (val logo : Int, val name : String)

