package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.DataModel.item

class CustomCartUploadPhotosAdapter (
        private val context : Context,
        private val itemList: List<item>
) : RecyclerView.Adapter<CustomCartUploadPhotosAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val uploadPhotosItemImageView : ImageView = view.findViewById(R.id.custom_cart_upload_photos_item_imageView)
        private val uploadPhotosDeleteItemImageView : ImageView = view.findViewById(R.id.custom_cart_upload_photos_item_deleteItem_imageView)


        fun bindView(i: item)         {
            uploadPhotosItemImageView.setImageResource(i.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
            LayoutInflater.from(context).inflate(R.layout.custom_cart_upload_photos_item, parent, false))

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(itemList[position])
    }
}