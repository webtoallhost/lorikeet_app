package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R


class ShippingAddressAdapter (
    private val context : Context,
    private val images: List<address>
) : RecyclerView.Adapter<ShippingAddressAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val img = itemView.findViewById<ImageView>(R.id.shipping_card_selection_imageView)
        val topic = itemView.findViewById<TextView>(R.id.shipping_card_title_textView)
        val descaddress = itemView.findViewById<TextView>(R.id.shipping_card_address_textView)
        val buttonViewOption=itemView.findViewById<ImageView>(R.id.shipping_card_option_imageView)

        fun bindView(image: address, context: Context) {

            img.setImageResource(image.image)
            topic.text= image.title
            descaddress.text=image.fulladdress
            buttonViewOption.setOnClickListener {
                //creating a popup menu

                //creating a popup menu
                val popup = PopupMenu(context, buttonViewOption)
                //inflating menu from xml resource
                //inflating menu from xml resource
                popup.inflate(R.menu.shipping_card_options_menu)
                //adding click listener
                //adding click listener
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.shipping_card_options_menu_edit -> {
                        }
                        R.id.shipping_card_options_menu_delete -> {
                        }
                    }
                    false
                }
                //displaying the popup
                //displaying the popup
                popup.show()
            }
        }
    }


    class address (val image: Int,val title : String, val fulladdress : String)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder =
        ImageViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.shipping_card,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position],context)
    }
}
