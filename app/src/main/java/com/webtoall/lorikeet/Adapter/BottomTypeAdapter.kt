package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.DataModel.SelectStyleDataModel
import com.webtoall.lorikeet.R

class BottomTypeAdapter (
        private val context : Context,
        private val images: List<SelectStyleDataModel>
) : RecyclerView.Adapter<BottomTypeAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val dressImage = itemView.findViewById<ImageView>(R.id.sleeve_type_recycler_view_item_dress_imageView)
        private val selectedImageView = itemView.findViewById<ImageView>(R.id.sleeve_type_recycler_view_item_selected_item_imageView)

        fun bindView(item: SelectStyleDataModel)         {
            dressImage.setImageResource(item.image)
            if(item.selected)
                selectedImageView.visibility= View.VISIBLE
            else
                selectedImageView.visibility= View.GONE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
            LayoutInflater.from(context).inflate(R.layout.sleeve_type_recycler_view_item, parent, false))

    override fun getItemCount(): Int = images.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(images[position])
    }
}