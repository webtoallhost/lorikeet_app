package com.webtoall.lorikeet.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.DataModel.item

class OrderProductPriceAdapter (
    private val context : Context,
    private val productPrice: List<item>
) : RecyclerView.Adapter<OrderProductPriceAdapter.ImageViewHolder>() {
    class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val productImage = itemView.findViewById<ImageView>(R.id.products_items_layout_productName_imageView)
        private val productName = itemView.findViewById<TextView>(R.id.products_items_layout_productName_textView)
        private val productPriceVal = itemView.findViewById<TextView>(R.id.products_items_layout_priceTotal_textView)

        fun bindView(productPrice: item)
        {
            productImage.setImageResource(productPrice.image)
            productName.text= productPrice.dressname
            productPriceVal.text=productPrice.price
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            ImageViewHolder = ImageViewHolder(
        LayoutInflater.from(context).inflate(R.layout.products_items_layout, parent, false))

    override fun getItemCount(): Int = productPrice.size

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindView(productPrice[position])
    }
}