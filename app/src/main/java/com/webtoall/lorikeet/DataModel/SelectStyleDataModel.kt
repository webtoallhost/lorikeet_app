package com.webtoall.lorikeet.DataModel

data class SelectStyleDataModel(val image:Int,val title:String,val selected:Boolean)