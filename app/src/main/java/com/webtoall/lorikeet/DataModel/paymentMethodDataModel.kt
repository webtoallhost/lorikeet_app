package com.webtoall.lorikeet.DataModel

data class PaymentMethodDataModel (val cardTypeImage: Int, val cardNumber : String, val selected : Boolean)