package com.webtoall.lorikeet.Model

import alirezat775.lib.carouselview.CarouselModel

class CarouselListModel constructor(private var id: Int) : CarouselModel() {

    fun getId(): Int {
        return id
    }
}