package com.webtoall.lorikeet.Model

import alirezat775.lib.carouselview.CarouselModel

class CarouselEmptyListModel constructor(private val text: String) : CarouselModel() {

    fun getText(): String {
        return text
    }
}