package com.webtoall.lorikeet.ruler;

public interface RulerValuePickerListener {

    void onValueChange(int selectedValue);

    void onIntermediateValueChange(int selectedValue);
}