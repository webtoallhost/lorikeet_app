package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatSpinner
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class StandardSize : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_standard_size)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar!!.customView
        val title = view.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Tailor Mens Standard"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val standardSizeBackButton = findViewById<AppCompatButton>(R.id.activity_standard_size_back_button)
        val standardSizeNextButton = findViewById<MaterialButton>(R.id.activity_standard_size_next_button)

        standardSizeBackButton.setOnClickListener {
            finish()
        }

        standardSizeNextButton.setOnClickListener {
            startActivity(Intent(this,CustomCart::class.java))
        }

        val selectSizeSpinner=findViewById<Spinner>(R.id.activity_standard_size_select_unit_spinner)
        val selectUnitImageView=findViewById<ImageView>(R.id.activity_standard_size_select_unit_imageView)
        val standardSizeSpinner=findViewById<AppCompatSpinner>(R.id.activity_standard_size_spinner)
        val standardSizeImageView =findViewById<ImageView>(R.id.activity_standard_size_imageView)

        val adapter: ArrayAdapter<*> = ArrayAdapter.createFromResource(
                this,
                R.array.collar_array,
                R.layout.color_spinner_layout
        )
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_layout)
        selectSizeSpinner.adapter = adapter
        selectSizeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        selectUnitImageView.setOnClickListener {
            selectSizeSpinner.performClick()
        }

/*
        val standardSizeAdapter: ArrayAdapter<*> = ArrayAdapter.createFromResource(
                this,
                R.array.size_measurement,
                R.layout.color_spinner_layout
        )
        standardSizeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_layout)
        standardSizeSpinner.adapter = standardSizeAdapter
        standardSizeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {

            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        standardSizeImageView.setOnClickListener {
            standardSizeSpinner.performClick()
        }*/


        /*val content = view.findViewById<View>(R.id.content)
        val label = view.findViewById<TextView>(R.id.label)
        // configuration of the shape for the outline
        val shape = ShapeAppearanceModel.Builder()
            .setAllCorners(RoundedCornerTreatment())
            .setAllCornerSizes(16f)
            .build()

        // configuration of the CutOutDrawable - replace with themed values instead of hard coded colors
        val drawable = CutoutDrawable(shape).apply {
            setStroke(40f, Color.BLACK)
            fillColor = ColorStateList.valueOf(Color.RED)
        }
        // content is the view with @+id/content
        content?.background = drawable
        // label is the view with @+id/label
        label?.addOnLayoutChangeListener { _, left, top, right, bottom, _, _, _, _ ->
            // offset the position by the margin of the content view
            val realLeft = left - content.left
            val realTop = top - content.top
            val realRigth = right - content.left
            val realBottom = bottom - content.top
            // update the cutout part of the drawable
            drawable.setCutout(
                realLeft.toFloat(),
                realTop.toFloat(),
                realRigth.toFloat(),
                realBottom.toFloat()
            )
     }*/
    }
}