package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.Adapter.OrderProductPriceAdapter
import com.webtoall.lorikeet.Adapter.PaymentMethodAdapter
import com.webtoall.lorikeet.DataModel.PaymentMethodDataModel
import com.webtoall.lorikeet.DataModel.item
import com.webtoall.lorikeet.R


class OrderSummary : AppCompatActivity() {

    var orderProductPriceRecyclerViewVisibility=true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_summary)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Order Summary"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val orderSummaryContinueButton = findViewById<MaterialButton>(R.id.activity_order_summary_continue_button)

        orderSummaryContinueButton.setOnClickListener {
           /* val checkOutThanks = CheckOutThanks(this,android.R.style.Theme)
            checkOutThanks.show()*/
            val displayRectangle = Rect()
            val window: Window = this.window
            window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
            val builder: AlertDialog.Builder = AlertDialog.Builder(this, R.style.CustomAlertDialog)
            val viewGroup = findViewById<ViewGroup>(android.R.id.content)
            val dialogView: View = LayoutInflater.from(it.context).inflate(R.layout.activity_check_out_thanks, viewGroup, false)
           /* dialogView.minimumWidth = (displayRectangle.width() * 1f).toInt()
            dialogView.minimumHeight = (displayRectangle.height() * 1f).toInt()*/

            //val yesButton=dialogView.findViewById<LinearLayout>(R.id.activity_check_out_thanks_if_yes_button_linearLayout)
            //val noButton=dialogView.findViewById<LinearLayout>(R.id.activity_check_out_thanks_if_no_button_linearLayout)
            val ratingsLinearLayout=dialogView.findViewById<LinearLayout>(R.id.activity_check_out_thanks_if_yes_linearLayout)
            val feedBackLinearLayout=dialogView.findViewById<LinearLayout>(R.id.activity_check_out_thanks_if_no_linearLayout)
            //val yesImageView=dialogView.findViewById<ImageView>(R.id.activity_check_out_thanks_if_yes_imageView)
            //val noImageView=dialogView.findViewById<ImageView>(R.id.activity_check_out_thanks_if_no_imageView)
            val yesTextView=dialogView.findViewById<TextView>(R.id.activity_check_out_thanks_if_yes_textView)
            val noTextView=dialogView.findViewById<TextView>(R.id.activity_check_out_thanks_if_no_textView)
            val checkOutThanksTrackOrderButton = dialogView.findViewById<MaterialButton>(R.id.activity_check_out_thanks_trackOrder_button)
            yesTextView.setTextColor(ContextCompat.getColor(this,R.color.white))
            noTextView.setTextColor(ContextCompat.getColor(this,R.color.white))
            yesTextView.setOnClickListener {

                yesTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.choose_language,0,0,0)
                noTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_button_unchecked,0,0,0)
                ratingsLinearLayout.visibility=View.VISIBLE
                feedBackLinearLayout.visibility=View.GONE
            }

            noTextView.setOnClickListener {
                yesTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.radio_button_unchecked,0,0,0)
                noTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.choose_language,0,0,0)
                ratingsLinearLayout.visibility=View.GONE
                feedBackLinearLayout.visibility=View.VISIBLE
            }


            builder.setView(dialogView)
            val alertDialog: AlertDialog = builder.create()
            alertDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
            /*val buttonOk: Button = dialogView.findViewById(R.id.buttonOk)
            buttonOk.setOnClickListener(View.OnClickListener { alertDialog.dismiss() })*/
            checkOutThanksTrackOrderButton.setOnClickListener {
                alertDialog.dismiss()
                startActivity(Intent(this,TrackOrder::class.java))
            }
            alertDialog.show()
        }


        val paymentMethodList = listOf(
            PaymentMethodDataModel(R.drawable.visa,".... .... .... .068",true),
            PaymentMethodDataModel(R.drawable.visa,".... .... .... .068",false)
        )
        val orderProductList = listOf(
            item(
                R.drawable.men,
                "Men's Shirt",
                "$30.00"
            ),
            item(
                R.drawable.sample1,
                "Men's T- Shirt",
                "$42.00"
            )
        )
        val orderProductPriceRecyclerView = findViewById<RecyclerView>(R.id.activity_order_summary_orderProductPrice_recyclerView)
        orderProductPriceRecyclerView.adapter= OrderProductPriceAdapter(this,orderProductList)

        val productDetailsSpinner = findViewById<TextView>(R.id.activity_order_summary_productSpinner_textView)
        productDetailsSpinner.setOnClickListener {
            if(orderProductPriceRecyclerViewVisibility){
                orderProductPriceRecyclerViewVisibility=false
                orderProductPriceRecyclerView.visibility=View.GONE
                productDetailsSpinner.setCompoundDrawablesWithIntrinsicBounds(0,0,
                    R.drawable.drop_down_list,0)
            }
            else{
                orderProductPriceRecyclerViewVisibility=true
                orderProductPriceRecyclerView.visibility=View.VISIBLE
                productDetailsSpinner.setCompoundDrawablesWithIntrinsicBounds(0,0,
                    R.drawable.drop_down_up_list,0)
            }
        }

        val paymentMethodRecyclerView = findViewById<RecyclerView>(R.id.activity_order_summary_paymentMethod_recyclerView)
        paymentMethodRecyclerView.adapter= PaymentMethodAdapter(this,paymentMethodList)

        val addNewTextView = findViewById<TextView>(R.id.activity_order_summary_addNew_textView)

        addNewTextView.setOnClickListener {
            val popup = PopupMenu(this, addNewTextView)
            popup.menuInflater.inflate(R.menu.payment_method_menu, popup.menu)
            popup.setOnMenuItemClickListener { menu_item ->
                when (menu_item.itemId) {
                    R.id.payment_method_menu_credit_card -> {
                    }
                    R.id.payment_method_menu_mobile_banking -> {
                    }
                    R.id.payment_method_menu_mobile_wallets -> {
                    }
                    R.id.payment_method_menu_cash_on_delivery -> {
                    }
                }
                true
            }
            val menuHelper = MenuPopupHelper(this, (popup.menu as MenuBuilder), addNewTextView)
            menuHelper.setForceShowIcon(true)
            menuHelper.gravity = Gravity.END
            menuHelper.show()
        }
    }
}