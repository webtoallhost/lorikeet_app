package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class SelectMeasurement : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_measurement)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar!!.customView
        val title = view.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Tailor Mens Style"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val selectMeasurementBodySizeViewButton = findViewById<MaterialButton>(R.id.activity_select_measurement_bodySize_view_button)
        val selectMeasurementStandardSizeViewButton = findViewById<MaterialButton>(R.id.activity_select_measurement_standardSize_view_button)
        val selectMeasurementRequestForProfessionalsViewButton = findViewById<MaterialButton>(R.id.activity_select_measurement_requestForProfessionals_view_button)


        selectMeasurementBodySizeViewButton.setOnClickListener {
            startActivity(Intent(this,BodySize::class.java))
        }
        selectMeasurementStandardSizeViewButton.setOnClickListener {
            startActivity(Intent(this,StandardSize::class.java))
        }
        selectMeasurementRequestForProfessionalsViewButton.setOnClickListener {
            startActivity(Intent(this,UploadPhotoListActivity::class.java))
        }
    }
}