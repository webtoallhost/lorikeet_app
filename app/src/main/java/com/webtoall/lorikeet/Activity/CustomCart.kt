package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.Adapter.CustomCartUploadPhotosAdapter
import com.webtoall.lorikeet.DataModel.item
import com.webtoall.lorikeet.R


class CustomCart : AppCompatActivity() {
    private var arrayList: ArrayList<item>?= null
    private var gridImageAdapter : CustomCartUploadPhotosAdapter?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom_cart)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Cart(04)"
        title.gravity= Gravity.CENTER
        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val customCartProceedToCheckoutButton = findViewById<MaterialButton>(R.id.activity_custom_cart_proceed_to_checkout_button)
        customCartProceedToCheckoutButton.setOnClickListener {
            startActivity(Intent(this,OrderSummary::class.java))
        }

        val spinner = findViewById<Spinner>(R.id.spinner)
        val adapter: ArrayAdapter<*> = ArrayAdapter.createFromResource(
            this,
            R.array.Languages,
            android.R.layout.simple_spinner_dropdown_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter


        val uploadPhotosRecyclerView = findViewById<RecyclerView>(R.id.activity_custom_cart_uploadPhotos_recyclerView)
        arrayList = ArrayList()

        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample3,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample3,
                "Dress Name Here",
                "$462.94"
            )
        )

        gridImageAdapter = CustomCartUploadPhotosAdapter(applicationContext, arrayList!!)
        uploadPhotosRecyclerView?.adapter = gridImageAdapter
    }
}