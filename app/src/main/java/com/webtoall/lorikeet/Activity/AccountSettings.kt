package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import com.webtoall.lorikeet.R

class AccountSettings : AppCompatActivity() {
    var settingsListView: ListView? = null
    var settingsList =
        arrayOf("Change Password", "Change Email")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_settings)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Account Settings"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        settingsListView = findViewById<ListView>(R.id.activity_account_settings_listView)
        val arrayAdapter = ArrayAdapter(
            this,
            R.layout.need_help_list_item,
            R.id.need_help_list_item_textView,
            settingsList
        )
        settingsListView!!.adapter = arrayAdapter

        settingsListView!!.setOnItemClickListener { parent, view, position, id ->
            //Toast.makeText(this, settingsList[position], Toast.LENGTH_SHORT).show()
            if(position==0){
                startActivity(Intent(this,ChangePassword::class.java))
            }
        }
    }
}