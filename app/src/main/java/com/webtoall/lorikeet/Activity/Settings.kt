package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import com.webtoall.lorikeet.R

class Settings : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Settings"
        title.gravity= Gravity.CENTER
        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val settingsPushNotificationLinearLayout=findViewById<LinearLayout>(R.id.activity_settings_push_notification_linearLayout)
        val settingsAccountSettingsLinearLayout=findViewById<LinearLayout>(R.id.activity_settings_account_settings_linearLayout)
        val settingsChangeLanguageLinearLayout=findViewById<LinearLayout>(R.id.activity_settings_change_language_linearLayout)
        val settingsLiveChatLinearLayout=findViewById<LinearLayout>(R.id.activity_settings_live_chat_linearLayout)
        val settingsNeedHelpLinearLayout=findViewById<LinearLayout>(R.id.activity_settings_need_help_linearLayout)

        settingsPushNotificationLinearLayout.setOnClickListener {
            startActivity(Intent(this,PushNotifications::class.java))
        }

        settingsAccountSettingsLinearLayout.setOnClickListener {
            startActivity(Intent(this,AccountSettings::class.java))
        }
        settingsChangeLanguageLinearLayout.setOnClickListener {
            startActivity(Intent(this,ChangeLanguage::class.java))
        }

        settingsLiveChatLinearLayout.setOnClickListener {
            //startActivity(Intent(this,Notifications::class.java))
            Toast.makeText(this,"No Navigation",Toast.LENGTH_SHORT).show()
        }
        settingsNeedHelpLinearLayout.setOnClickListener {
            startActivity(Intent(this,NeedHelp::class.java))
        }



        supportActionBar?.title = "Ripple Animations"
    }

    fun checkRippleAnimation(view: View) {}
    fun dummyClick(view: View) {



    }
}