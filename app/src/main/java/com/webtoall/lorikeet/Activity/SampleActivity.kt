package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.updatePadding
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mindinventory.midrawer.MIDrawerView
import com.mindinventory.midrawer.MIDrawerView.Companion.MI_TYPE_DOOR_IN
import com.mindinventory.midrawer.MIDrawerView.Companion.MI_TYPE_DOOR_OUT
import com.mindinventory.midrawer.MIDrawerView.Companion.MI_TYPE_SLIDE
import com.mindinventory.midrawer.MIDrawerView.Companion.MI_TYPE_SLIDE_WITH_CONTENT
import com.webtoall.lorikeet.Fragment.*
import com.webtoall.lorikeet.R

open class SampleActivity : AppCompatActivity(), View.OnClickListener {
    val TAG = "DrawerActivity"
    private var slideType = 0

    var drawerLayout:MIDrawerView?=null
    var navScroll:TextView?=null
    var navSlide:TextView?=null
    var navDoorIn:TextView?=null
    var navDoorOut:TextView?=null
    var navHomeVal:TextView?=null
    var navMyOrdersVal:TextView?=null
    var navSettingsVal:TextView?=null
    var navRegisterAsStoreVal:TextView?=null
    var navChatsVal:TextView?=null
    var navTermsAndConditionsVal:TextView?=null
    var navPrivacyPolicyVal:TextView?=null
    var navAboutUsVal:TextView?=null
    var toolBar:Toolbar?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)

       /* supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_layout)
        //supportActionBar?.setHomeAsUpIndicator(R.id.action_bar_back)
        val view = supportActionBar!!.customView
        val actionBarBack=view.findViewById<ImageButton>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            drawerLayout!!.openDrawer(GravityCompat.START)
        }*/


        drawerLayout=findViewById<MIDrawerView>(R.id.drawer_layout)
       /* navScroll=findViewById<TextView>(R.id.nav_scroll)
        navSlide=findViewById<TextView>(R.id.nav_slide)
        navDoorIn=findViewById<TextView>(R.id.nav_doorIn)
        navDoorOut=findViewById<TextView>(R.id.nav_doorOut)*/
        navHomeVal=findViewById<TextView>(R.id.navHome)
        navMyOrdersVal=findViewById<TextView>(R.id.navMyOrders)
        navSettingsVal=findViewById<TextView>(R.id.navSettings)
        navRegisterAsStoreVal=findViewById<TextView>(R.id.navRegisterAsStore)
        navChatsVal=findViewById<TextView>(R.id.navChats)
        navTermsAndConditionsVal=findViewById<TextView>(R.id.navTermsAndConditions)
        navPrivacyPolicyVal=findViewById<TextView>(R.id.navPrivacyPolicy)
        navAboutUsVal=findViewById<TextView>(R.id.navAboutUs)
        toolBar=findViewById<Toolbar>(R.id.toolbar)

        // Set color for the container's content as transparent
        drawerLayout!!.setScrimColor(Color.TRANSPARENT)

        /*navScroll!!.setOnClickListener(this)
        navSlide!!.setOnClickListener(this)
        navDoorIn!!.setOnClickListener(this)
        navDoorOut!!.setOnClickListener(this)*/
        navHomeVal!!.setOnClickListener(this)
        navMyOrdersVal!!.setOnClickListener(this)
        navSettingsVal!!.setOnClickListener(this)
        navRegisterAsStoreVal!!.setOnClickListener(this)
        navChatsVal!!.setOnClickListener(this)
        navTermsAndConditionsVal!!.setOnClickListener(this)
        navPrivacyPolicyVal!!.setOnClickListener(this)
        navAboutUsVal!!.setOnClickListener(this)

        /*setSupportActionBar(toolBar)

        val actionbar = supportActionBar
        actionbar?.setDisplayHomeAsUpEnabled(true)
        actionbar?.setHomeAsUpIndicator(R.drawable.ic_action_name)*/


        // Implement the drawer listener
        drawerLayout?.setMIDrawerListener(object : MIDrawerView.MIDrawerEvents {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                Log.d(TAG, "Drawer Opened")
            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                Log.d(TAG, "Drawer closed")
            }
        })
        val mainHomeBottomNavigation =
            findViewById<BottomNavigationView>(R.id.activity_main_home_bottom_navigation)

        val firstFragment = HomeFragment()
        val secondFragment = ShowRoomFragment()
        val thirdFragment = TailorFragment()

        setCurrentFragment(firstFragment)

        mainHomeBottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.bottomNav_home -> {
                    Toast.makeText(this,"FirstFragment", Toast.LENGTH_SHORT).show()
                    setCurrentFragment(firstFragment)}
                R.id.bottomNav_store -> setCurrentFragment(secondFragment)
                R.id.bottomNav_cut -> setCurrentFragment(thirdFragment)
                R.id.bottomNav_favourites -> setCurrentFragment(FavouritesFragment())
                R.id.bottomNav_profile -> setCurrentFragment(MyProfileFragment())
            }
            true
        }
    }

    override fun onBackPressed() {
        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
           /* R.id.nav_scroll -> {
                avoidDoubleClicks(navScroll!!)
                slideType = MI_TYPE_SLIDE_WITH_CONTENT
                updateSliderTypeEvents()
            }
            R.id.nav_slide -> {
                avoidDoubleClicks(navSlide!!)
                slideType = MI_TYPE_SLIDE
                updateSliderTypeEvents()
            }
            R.id.nav_doorIn -> {
                avoidDoubleClicks(navDoorIn!!)
                slideType = MI_TYPE_DOOR_IN
                updateSliderTypeEvents()
            }
            R.id.nav_doorOut -> {
                avoidDoubleClicks(navDoorOut!!)
                slideType = MI_TYPE_DOOR_OUT
                updateSliderTypeEvents()
            }*/
            R.id.navHome -> {
                avoidDoubleClicks(navHomeVal!!)
                //slideType = MI_TYPE_SLIDE_WITH_CONTENT
                //updateSliderTypeEvents()
                navHomeVal!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected_item,0,0,0)
                navHomeVal!!.alpha=1.0f
                navHomeVal!!.setPadding(0,0,0,15)
                navHomeVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.blue
                ))

                navMyOrdersVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navMyOrdersVal!!.alpha=0.5f
                navMyOrdersVal!!.updatePadding(45,0,0,50)
                navMyOrdersVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navSettingsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navSettingsVal!!.alpha=0.5f
                navSettingsVal!!.setPadding(45,0,0,50)
                navSettingsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navRegisterAsStoreVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navRegisterAsStoreVal!!.alpha=0.5f
                navRegisterAsStoreVal!!.setPadding(45,0,0,50)
                navRegisterAsStoreVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navChatsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navChatsVal!!.alpha=0.5f
                navChatsVal!!.updatePadding(45,0,0,50)
                navChatsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navTermsAndConditionsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navTermsAndConditionsVal!!.alpha=0.5f
                navTermsAndConditionsVal!!.setPadding(45,0,0,50)
                navTermsAndConditionsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navPrivacyPolicyVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navPrivacyPolicyVal!!.alpha=0.5f
                navPrivacyPolicyVal!!.setPadding(45,0,0,50)
                navPrivacyPolicyVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navAboutUsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navAboutUsVal!!.alpha=0.5f
                navAboutUsVal!!.setPadding(45,0,0,50)
                navAboutUsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))
                drawerLayout!!.closeDrawer(GravityCompat.START)
            }
            R.id.navMyOrders -> {
                avoidDoubleClicks(navMyOrdersVal!!)
                //slideType = MI_TYPE_SLIDE
                //updateSliderTypeEvents()
                navMyOrdersVal!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected_item,0,0,0)
                navMyOrdersVal!!.alpha=1.0f
                navMyOrdersVal!!.setPadding(0,0,0,15)
                navMyOrdersVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.blue
                ))

                navHomeVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navHomeVal!!.alpha=0.5f
                navHomeVal!!.updatePadding(45,0,0,15)
                navHomeVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navSettingsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navSettingsVal!!.alpha=0.5f
                navSettingsVal!!.setPadding(45,0,0,50)
                navSettingsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navRegisterAsStoreVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navRegisterAsStoreVal!!.alpha=0.5f
                navRegisterAsStoreVal!!.setPadding(45,0,0,50)
                navRegisterAsStoreVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navChatsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navChatsVal!!.alpha=0.5f
                navChatsVal!!.updatePadding(45,0,0,50)
                navChatsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navTermsAndConditionsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navTermsAndConditionsVal!!.alpha=0.5f
                navTermsAndConditionsVal!!.setPadding(45,0,0,50)
                navTermsAndConditionsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navPrivacyPolicyVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navPrivacyPolicyVal!!.alpha=0.5f
                navPrivacyPolicyVal!!.setPadding(45,0,0,50)
                navPrivacyPolicyVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navAboutUsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navAboutUsVal!!.alpha=0.5f
                navAboutUsVal!!.setPadding(45,0,0,50)
                navAboutUsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))
                drawerLayout!!.closeDrawer(GravityCompat.START)
                startActivity(Intent(this,
                    MyOrders::class.java))

            }
            R.id.navSettings -> {
                avoidDoubleClicks(navSettingsVal!!)
                //slideType = MI_TYPE_DOOR_IN
                //updateSliderTypeEvents()
                navSettingsVal!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected_item,0,0,0)
                navSettingsVal!!.alpha=1.0f
                navSettingsVal!!.setPadding(0,0,0,15)
                navSettingsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.blue
                ))

                navMyOrdersVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navMyOrdersVal!!.alpha=0.5f
                navMyOrdersVal!!.updatePadding(45,50,0,15)
                navMyOrdersVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navHomeVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navHomeVal!!.alpha=0.5f
                navHomeVal!!.updatePadding(45,0,0,15)
                navHomeVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navRegisterAsStoreVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navRegisterAsStoreVal!!.alpha=0.5f
                navRegisterAsStoreVal!!.setPadding(45,0,0,50)
                navRegisterAsStoreVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navChatsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navChatsVal!!.alpha=0.5f
                navChatsVal!!.updatePadding(45,0,0,50)
                navChatsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navTermsAndConditionsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navTermsAndConditionsVal!!.alpha=0.5f
                navTermsAndConditionsVal!!.setPadding(45,0,0,50)
                navTermsAndConditionsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navPrivacyPolicyVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navPrivacyPolicyVal!!.alpha=0.5f
                navPrivacyPolicyVal!!.setPadding(45,0,0,50)
                navPrivacyPolicyVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navAboutUsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navAboutUsVal!!.alpha=0.5f
                navAboutUsVal!!.setPadding(45,0,0,50)
                navAboutUsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                drawerLayout!!.closeDrawer(GravityCompat.START)
                startActivity(Intent(this,Settings::class.java))
            }
            R.id.navRegisterAsStore -> {
                avoidDoubleClicks(navRegisterAsStoreVal!!)
                //slideType = MI_TYPE_DOOR_OUT
                //updateSliderTypeEvents()
                navRegisterAsStoreVal!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected_item,0,0,0)
                navRegisterAsStoreVal!!.alpha=1.0f
                navRegisterAsStoreVal!!.setPadding(0,0,0,15)
                navRegisterAsStoreVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.blue
                ))

                navMyOrdersVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navMyOrdersVal!!.alpha=0.5f
                navMyOrdersVal!!.updatePadding(45,50,0,15)
                navMyOrdersVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navHomeVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navHomeVal!!.alpha=0.5f
                navHomeVal!!.updatePadding(45,0,0,15)
                navHomeVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navSettingsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navSettingsVal!!.alpha=0.5f
                navSettingsVal!!.updatePadding(45,50,0,15)
                navSettingsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navChatsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navChatsVal!!.alpha=0.5f
                navChatsVal!!.updatePadding(45,0,0,50)
                navChatsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navTermsAndConditionsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navTermsAndConditionsVal!!.alpha=0.5f
                navTermsAndConditionsVal!!.setPadding(45,0,0,50)
                navTermsAndConditionsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navPrivacyPolicyVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navPrivacyPolicyVal!!.alpha=0.5f
                navPrivacyPolicyVal!!.setPadding(45,0,0,50)
                navPrivacyPolicyVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navAboutUsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navAboutUsVal!!.alpha=0.5f
                navAboutUsVal!!.setPadding(45,0,0,50)
                navAboutUsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                drawerLayout!!.closeDrawer(GravityCompat.START)
                startActivity(Intent(this,RegisterAsStore::class.java))
            }
            R.id.navChats -> {
                avoidDoubleClicks(navChatsVal!!)
                //slideType = MI_TYPE_SLIDE_WITH_CONTENT
                //updateSliderTypeEvents()
                navChatsVal!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected_item,0,0,0)
                navChatsVal!!.alpha=1.0f
                navChatsVal!!.setPadding(0,0,0,15)
                navChatsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.blue
                ))

                navHomeVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navHomeVal!!.alpha=0.5f
                navHomeVal!!.updatePadding(45,0,0,15)
                navHomeVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navMyOrdersVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navMyOrdersVal!!.alpha=0.5f
                navMyOrdersVal!!.updatePadding(45,50,0,15)
                navMyOrdersVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navSettingsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navSettingsVal!!.alpha=0.5f
                navSettingsVal!!.setPadding(45,50,0,15)
                navSettingsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navRegisterAsStoreVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navRegisterAsStoreVal!!.alpha=0.5f
                navRegisterAsStoreVal!!.setPadding(45,50,0,15)
                navRegisterAsStoreVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navTermsAndConditionsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navTermsAndConditionsVal!!.alpha=0.5f
                navTermsAndConditionsVal!!.setPadding(45,0,0,50)
                navTermsAndConditionsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navPrivacyPolicyVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navPrivacyPolicyVal!!.alpha=0.5f
                navPrivacyPolicyVal!!.setPadding(45,0,0,50)
                navPrivacyPolicyVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navAboutUsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navAboutUsVal!!.alpha=0.5f
                navAboutUsVal!!.setPadding(45,0,0,50)
                navAboutUsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                /*drawerLayout!!.closeDrawer(GravityCompat.START)
                startActivity(Intent(this,Settings::class.java))*/
            }
            R.id.navTermsAndConditions -> {
                avoidDoubleClicks(navTermsAndConditionsVal!!)
                //slideType = MI_TYPE_SLIDE
                //updateSliderTypeEvents()
                navTermsAndConditionsVal!!.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.selected_item,0,0,0)
                navTermsAndConditionsVal!!.alpha=1.0f
                navTermsAndConditionsVal!!.setPadding(0,0,0,15)
                navTermsAndConditionsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.blue
                ))

                navHomeVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navHomeVal!!.alpha=0.5f
                navHomeVal!!.updatePadding(45,0,0,15)
                navHomeVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navMyOrdersVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navMyOrdersVal!!.alpha=0.5f
                navMyOrdersVal!!.updatePadding(45,50,0,15)
                navMyOrdersVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navSettingsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navSettingsVal!!.alpha=0.5f
                navSettingsVal!!.setPadding(45,50,0,15)
                navSettingsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navRegisterAsStoreVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navRegisterAsStoreVal!!.alpha=0.5f
                navRegisterAsStoreVal!!.setPadding(45,50,0,15)
                navRegisterAsStoreVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navChatsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navChatsVal!!.alpha=0.5f
                navChatsVal!!.setPadding(45,50,0,15)
                navChatsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navPrivacyPolicyVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navPrivacyPolicyVal!!.alpha=0.5f
                navPrivacyPolicyVal!!.setPadding(45,0,0,50)
                navPrivacyPolicyVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navAboutUsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navAboutUsVal!!.alpha=0.5f
                navAboutUsVal!!.setPadding(45,0,0,50)
                navAboutUsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                drawerLayout!!.closeDrawer(GravityCompat.START)
                startActivity(Intent(this,
                    TermsAndConditions::class.java))
            }
            R.id.navPrivacyPolicy -> {
                avoidDoubleClicks(navPrivacyPolicyVal!!)
                //slideType = MI_TYPE_DOOR_IN
                //updateSliderTypeEvents()
                navPrivacyPolicyVal!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected_item,0,0,0)
                navPrivacyPolicyVal!!.alpha=1.0f
                navPrivacyPolicyVal!!.setPadding(0,0,0,15)
                navPrivacyPolicyVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.blue
                ))

                navHomeVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navHomeVal!!.alpha=0.5f
                navHomeVal!!.updatePadding(45,0,0,15)
                navHomeVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navMyOrdersVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navMyOrdersVal!!.alpha=0.5f
                navMyOrdersVal!!.updatePadding(45,50,0,15)
                navMyOrdersVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navSettingsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navSettingsVal!!.alpha=0.5f
                navSettingsVal!!.setPadding(45,50,0,15)
                navSettingsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navRegisterAsStoreVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navRegisterAsStoreVal!!.alpha=0.5f
                navRegisterAsStoreVal!!.setPadding(45,50,0,15)
                navRegisterAsStoreVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navChatsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navChatsVal!!.alpha=0.5f
                navChatsVal!!.setPadding(45,50,0,15)
                navChatsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navTermsAndConditionsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navTermsAndConditionsVal!!.alpha=0.5f
                navTermsAndConditionsVal!!.setPadding(45,50,0,15)
                navTermsAndConditionsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navAboutUsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navAboutUsVal!!.alpha=0.5f
                navAboutUsVal!!.setPadding(45,0,0,50)
                navAboutUsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                drawerLayout!!.closeDrawer(GravityCompat.START)
                startActivity(Intent(this,
                    PrivacyPolicy::class.java))
            }
            R.id.navAboutUs -> {
                avoidDoubleClicks(navAboutUsVal!!)
                //slideType = MI_TYPE_DOOR_OUT
                //updateSliderTypeEvents()
                navAboutUsVal!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected_item,0,0,0)
                navAboutUsVal!!.alpha=1.0f
                navAboutUsVal!!.setPadding(0,0,0,15)
                navAboutUsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.blue
                ))

                navHomeVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navHomeVal!!.alpha=0.5f
                navHomeVal!!.updatePadding(45,0,0,15)
                navHomeVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navMyOrdersVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navMyOrdersVal!!.alpha=0.5f
                navMyOrdersVal!!.updatePadding(45,50,0,15)
                navMyOrdersVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navSettingsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navSettingsVal!!.alpha=0.5f
                navSettingsVal!!.setPadding(45,50,0,15)
                navSettingsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navRegisterAsStoreVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navRegisterAsStoreVal!!.alpha=0.5f
                navRegisterAsStoreVal!!.setPadding(45,50,0,15)
                navRegisterAsStoreVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navChatsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navChatsVal!!.alpha=0.5f
                navChatsVal!!.setPadding(45,50,50,15)
                navChatsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navTermsAndConditionsVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navTermsAndConditionsVal!!.alpha=0.5f
                navTermsAndConditionsVal!!.setPadding(45,50,0,15)
                navTermsAndConditionsVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

                navPrivacyPolicyVal!!.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0)
                navPrivacyPolicyVal!!.alpha=0.5f
                navPrivacyPolicyVal!!.setPadding(45,50,0,15)
                navPrivacyPolicyVal!!.setTextColor(ContextCompat.getColor(applicationContext,
                    R.color.black
                ))

               /* drawerLayout!!.closeDrawer(GravityCompat.START)
                startActivity(Intent(this,
                    NeedHelp::class.java))*/
            }
        }
    }

    private fun updateSliderTypeEvents() {
        if (handler == null) {
            handler = Handler()
            drawerLayout!!.closeDrawer(GravityCompat.START)
            handler?.postDelayed(runnable, 500)
        }
    }

    var handler: Handler? = null
    var runnable: Runnable = Runnable {
        when (slideType) {
            MI_TYPE_SLIDE_WITH_CONTENT -> {
                //toolBar!!.title = this.resources.getString(R.string.scroll)
            }
            MI_TYPE_SLIDE -> {
                //toolBar!!.title = this.resources.getString(R.string.slide)
            }
            MI_TYPE_DOOR_IN -> {
                //toolBar!!.title = this.resources.getString(R.string.doorIn)
            }
            MI_TYPE_DOOR_OUT -> {
                //toolBar!!.title = this.resources.getString(R.string.doorOut)
            }
        }
        drawerLayout!!.setSliderType(slideType)
        handler = null
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                drawerLayout!!.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Avoid double click.
     */
    fun avoidDoubleClicks(view: View) {
        val DELAY_IN_MS: Long = 900
        if (!view.isClickable) {
            return
        }
        view.isClickable = false
        view.postDelayed({ view.isClickable = true }, DELAY_IN_MS)
    }

    private fun setCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.activity_main_home_frameLayout, fragment)
            commit()
        }
}