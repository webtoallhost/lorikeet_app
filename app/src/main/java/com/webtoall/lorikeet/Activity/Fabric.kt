package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Adapter.ProductsAdapter
import com.webtoall.lorikeet.Adapter.ShapesAdapter
import com.webtoall.lorikeet.Adapter.color1
import com.webtoall.lorikeet.DataModel.item
import com.webtoall.lorikeet.R

class Fabric : AppCompatActivity() {


    private var arrival_recyclerView: RecyclerView?=null
    private var products_recycleView_color:RecyclerView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fabric)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Blue Jean Fabric"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        arrival_recyclerView=findViewById(R.id.arrival_recyclerView)
        products_recycleView_color=findViewById(R.id.products_recycleView_color)


        val items = listOf(
            item(
                R.drawable.model1,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model2,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model1,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model2,
                "Dress Name Here",
                "$516.44"
            )
        )

        arrival_recyclerView?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        arrival_recyclerView?.setHasFixedSize(true)
        arrival_recyclerView?.adapter=
            ProductsAdapter(this, items)


        val color1=listOf(
            color1(R.drawable.color_background),
            color1(R.drawable.color_background1),
            color1(R.drawable.color_background2),
            color1(R.drawable.color_background3)
        )

        products_recycleView_color?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        products_recycleView_color?.setHasFixedSize(true)
        products_recycleView_color?.adapter=
            ShapesAdapter(this, color1)



    }
}