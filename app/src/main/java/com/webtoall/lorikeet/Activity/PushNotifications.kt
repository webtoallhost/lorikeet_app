package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.github.angads25.toggle.widget.LabeledSwitch
import com.webtoall.lorikeet.R


class PushNotifications : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_push_notifications)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Push Notifications"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }


        val offersLinearLayout=findViewById<LinearLayout>(R.id.activity_push_notifications_offers_linearLayout)
        val offersSwitch=findViewById<LabeledSwitch>(R.id.activity_push_notifications_offers_switch)
        val messageFromAdminLinearLayout=findViewById<LinearLayout>(R.id.activity_push_notifications_messageFromAdmin_linearLayout)
        val messageFromAdminSwitch=findViewById<LabeledSwitch>(R.id.activity_push_notifications_messageFromAdmin_switch)
        val orderNotificationsLinearLayout=findViewById<LinearLayout>(R.id.activity_push_notifications_orderNotifications_linearLayout)
        val orderNotificationsSwitch=findViewById<LabeledSwitch>(R.id.activity_push_notifications_orderNotifications_switch)
        val appointmentNotificationsLinearLayout=findViewById<LinearLayout>(R.id.activity_push_notifications_appointmentNotifications_linearLayout)
        val appointmentNotificationsSwitch=findViewById<LabeledSwitch>(R.id.activity_push_notifications_appointmentNotifications_switch)

        offersLinearLayout.setOnClickListener {
            offersSwitch.isOn = !offersSwitch.isOn
        }

        messageFromAdminLinearLayout.setOnClickListener {
            messageFromAdminSwitch.isOn = !messageFromAdminSwitch.isOn
        }

        orderNotificationsLinearLayout.setOnClickListener {
            orderNotificationsSwitch.isOn = !orderNotificationsSwitch.isOn
        }

        appointmentNotificationsLinearLayout.setOnClickListener {
            appointmentNotificationsSwitch.isOn = !appointmentNotificationsSwitch.isOn
        }
    }
}