package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.Adapter.*
import com.webtoall.lorikeet.DataModel.SelectStyleDataModel
import com.webtoall.lorikeet.R

class SelectStyle : AppCompatActivity() {

    private var sleeveTypeArrayList: ArrayList<SelectStyleDataModel>?= null
    private var sleeveTypeAdapter : SleeveTypeAdapter?= null
    private var bottomTypeArrayList: ArrayList<SelectStyleDataModel>?= null
    private var bottomTypeAdapter : BottomTypeAdapter?= null

    private var headCollarArrayList: ArrayList<SelectStyleDataModel>?= null
    private var headCollarAdapter : HeadCollarAdapter?= null
    private var handCollarArrayList: ArrayList<SelectStyleDataModel>?= null
    private var handCollarAdapter : HandCollarAdapter?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_style)
        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar!!.customView
        val title = view.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Tailor Mens Style"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val selectStyleBackButton = findViewById<AppCompatButton>(R.id.activity_select_style_back_button)
        val selectStyleNextButton = findViewById<MaterialButton>(R.id.activity_select_style_next_button)
        selectStyleBackButton.setOnClickListener {
            finish()
        }

        selectStyleNextButton.setOnClickListener {
            startActivity(Intent(this,FinalYourProduct::class.java))
        }

        val sleeveTypeRecyclerView = findViewById<RecyclerView>(R.id.activity_select_style_sleeveType_recyclerView)
        val bottomTypeRecyclerView = findViewById<RecyclerView>(R.id.activity_select_style_bottomType_recyclerView)
        val headCollarRecyclerView = findViewById<RecyclerView>(R.id.activity_select_style_headCollar_recyclerView)
        val handCollarRecyclerView = findViewById<RecyclerView>(R.id.activity_select_style_handCollar_recyclerView)
        sleeveTypeArrayList = ArrayList()
        bottomTypeArrayList = ArrayList()
        headCollarArrayList= ArrayList()
        handCollarArrayList= ArrayList()

        sleeveTypeArrayList?.add(SelectStyleDataModel(R.drawable.sleeve_type_1,"",false))
        sleeveTypeArrayList?.add(SelectStyleDataModel(R.drawable.sleeve_type_2,"",true))
        sleeveTypeArrayList?.add(SelectStyleDataModel(R.drawable.sleeve_type_3,"",false))

        bottomTypeArrayList?.add(SelectStyleDataModel(R.drawable.sleeve_type_1,"",true))
        bottomTypeArrayList?.add(SelectStyleDataModel(R.drawable.sleeve_type_2,"",false))
        bottomTypeArrayList?.add(SelectStyleDataModel(R.drawable.sleeve_type_3,"",false))

        headCollarArrayList?.add(SelectStyleDataModel(R.drawable.one,"CLASSIC",false))
        headCollarArrayList?.add(SelectStyleDataModel(R.drawable.two,"BOTTOM DOWN",false))
        headCollarArrayList?.add(SelectStyleDataModel(R.drawable.three,"SPREAD",true))
        headCollarArrayList?.add(SelectStyleDataModel(R.drawable.four,"CLUB",false))
        headCollarArrayList?.add(SelectStyleDataModel(R.drawable.five,"MANDARIN",false))

        handCollarArrayList?.add(SelectStyleDataModel(R.drawable.one_,"ONE BUTTON",true))
        handCollarArrayList?.add(SelectStyleDataModel(R.drawable.two_,"TWO BUTTONs",false))
        handCollarArrayList?.add(SelectStyleDataModel(R.drawable.three_,"ROUNDED",false))
        handCollarArrayList?.add(SelectStyleDataModel(R.drawable.four_,"FRENCH",false))
        handCollarArrayList?.add(SelectStyleDataModel(R.drawable.five_,"ANGLE CUT",false))

        sleeveTypeAdapter = SleeveTypeAdapter(applicationContext, sleeveTypeArrayList!!)
        sleeveTypeRecyclerView?.adapter = sleeveTypeAdapter

        bottomTypeAdapter = BottomTypeAdapter(applicationContext, bottomTypeArrayList!!)
        bottomTypeRecyclerView?.adapter = bottomTypeAdapter

        headCollarAdapter = HeadCollarAdapter(applicationContext, headCollarArrayList!!)
        headCollarRecyclerView?.adapter = headCollarAdapter

        handCollarAdapter = HandCollarAdapter(applicationContext, handCollarArrayList!!)
        handCollarRecyclerView?.adapter = handCollarAdapter
    }
}