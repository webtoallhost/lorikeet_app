package com.webtoall.lorikeet.Activity

import alirezat775.lib.carouselview.Carousel
import alirezat775.lib.carouselview.CarouselLazyLoadListener
import alirezat775.lib.carouselview.CarouselListener
import alirezat775.lib.carouselview.CarouselView
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatSpinner
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import com.webtoall.lorikeet.Adapter.BodySizeCarouselListAdapter
import com.webtoall.lorikeet.Model.CarouselListModel
import com.webtoall.lorikeet.R

class BodySize : AppCompatActivity() {

    private var hasNextPage: Boolean = true
    val TAG: String = this::class.java.name
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_body_size)
        val adapter = BodySizeCarouselListAdapter()
        val carousel = Carousel(this, findViewById(R.id.activity_body_size_carousel_view), adapter)
        carousel.setOrientation(CarouselView.HORIZONTAL, false)
        carousel.autoScroll(false, 5000, true)
        carousel.scaleView(true)
        carousel.lazyLoad(true, object : CarouselLazyLoadListener {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: CarouselView) {
                if (hasNextPage) {
                    Log.d(TAG, "load new item on lazy mode")
                    carousel.add(CarouselListModel(11))
                    carousel.add(CarouselListModel(12))
                    carousel.add(CarouselListModel(13))
                    carousel.add(CarouselListModel(14))
                    carousel.add(CarouselListModel(15))
                    hasNextPage = false
                }
            }
        })
        adapter.setOnClickListener(object :
            BodySizeCarouselListAdapter.OnClick {
            override fun click(model: CarouselListModel) {
                carousel.remove(model)
            }
        })

        carousel.addCarouselListener(object : CarouselListener {
            override fun onPositionChange(position: Int) {
                Log.d(TAG, "currentPosition : $position")
            }

            override fun onScroll(dx: Int, dy: Int) {
                Log.d(TAG, "onScroll dx : $dx -- dy : $dx")
            }
        })
        carousel.add(CarouselListModel(1))
        carousel.add(CarouselListModel(2))
        carousel.add(CarouselListModel(3))
        carousel.add(CarouselListModel(4))
        carousel.add(CarouselListModel(5))
        carousel.add(CarouselListModel(6))
        carousel.add(CarouselListModel(7))
        carousel.add(CarouselListModel(8))
        carousel.add(CarouselListModel(9))
        carousel.add(CarouselListModel(10))
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Tailor Mens Measurement"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val bodySizeBackButton = findViewById<AppCompatButton>(R.id.activity_body_size_back_button)
        val bodySizeNextButton = findViewById<MaterialButton>(R.id.activity_body_size_next_button)

        bodySizeBackButton.setOnClickListener {
            finish()
        }

        bodySizeNextButton.setOnClickListener {
            startActivity(Intent(this,CustomCart::class.java))
        }


        val selectSizeSpinner=findViewById<Spinner>(R.id.activity_body_size_select_unit_spinner)
        val selectUnitImageView=findViewById<ImageView>(R.id.activity_body_size_select_unit_imageView)
        val neckSizeTxtInLayout=findViewById<TextInputLayout>(R.id.activity_body_size_neck_size_txtInLayout)
        val chestSizeTxtInLayout=findViewById<TextInputLayout>(R.id.activity_body_size_chest_size_txtInLayout)
        val stomachSizeTxtInLayout=findViewById<TextInputLayout>(R.id.activity_body_size_stomach_size_txtInLayout)
        val hipSizeTxtInLayout=findViewById<TextInputLayout>(R.id.activity_body_size_hip_size_txtInLayout)
        val shoulderSizeTxtInLayout=findViewById<TextInputLayout>(R.id.activity_body_size_shoulder_size_txtInLayout)
        val sleeveSizeTxtInLayout=findViewById<TextInputLayout>(R.id.activity_body_size_sleeve_size_txtInLayout)
        val lengthSizeTxtInLayout=findViewById<TextInputLayout>(R.id.activity_body_size_length_size_txtInLayout)

        val selectSizeAdapter: ArrayAdapter<*> = ArrayAdapter.createFromResource(
                this,
                R.array.collar_array,
                R.layout.color_spinner_layout
        )
        selectSizeAdapter.setDropDownViewResource(R.layout.spinner_dropdown_layout)
        selectSizeSpinner.adapter = selectSizeAdapter
        selectSizeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                neckSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
                chestSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
                stomachSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
                hipSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
                shoulderSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
                sleeveSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
                lengthSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        selectUnitImageView.setOnClickListener {
            selectSizeSpinner.performClick()
        }


    }
}