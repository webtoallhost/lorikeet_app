package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.TimeLine.Model.OrderStatus
import com.webtoall.lorikeet.TimeLine.Model.TimeLineModel
import com.webtoall.lorikeet.TimeLine.example.ExampleTimeLineAdapter
import java.util.ArrayList

class TrackOrder : AppCompatActivity() {

    private lateinit var mAdapter: ExampleTimeLineAdapter
    private val mDataList = ArrayList<TimeLineModel>()
    private lateinit var mLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_order)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Track Order"
        title.gravity= Gravity.CENTER
        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }
        //setActivityTitle(getString(R.string.activity_example_label))
        //isDisplayHomeAsUpEnabled = true

        setDataListItems()
        initRecyclerView()
    }

    private fun setDataListItems() {
        mDataList.add(TimeLineModel("Confirm Order", "2020-08-28 16:30", OrderStatus.COMPLETED,1))
        mDataList.add(TimeLineModel("Product Prepared", "2020-08-30 02:00", OrderStatus.ACTIVE,2))
        mDataList.add(TimeLineModel("Shipped", "2020-09-01 14:10", OrderStatus.INACTIVE,3))
        mDataList.add(TimeLineModel("Out For Delivery", "2020-09-02 10:25", OrderStatus.INACTIVE,3))
        mDataList.add(TimeLineModel("Delivered", "2020-09-03 16:25", OrderStatus.INACTIVE,4))
        //mDataList.add(TimeLineModel("Order placed successfully", "2017-02-10 14:00", OrderStatus.INACTIVE))
    }

    private fun initRecyclerView() {
        val recyclerView=findViewById<RecyclerView>(R.id.recyclerView)
        mLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = mLayoutManager
        mAdapter = ExampleTimeLineAdapter(mDataList)
        recyclerView.adapter = mAdapter
    }
}