package com.webtoall.lorikeet.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class SignInActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        val skipTextView = findViewById<AppCompatTextView>(R.id.activity_signIn_skip_textView)
        val createYourAccountTextView = findViewById<TextView>(R.id.activity_signIn_createYourAccount_textView)
        val forgetPasswordTextView=findViewById<AppCompatTextView>(R.id.activity_signIn_forgetPassword_textView)
        val signInButton=findViewById<MaterialButton>(R.id.activity_signIn_button)
        skipTextView.setOnClickListener {
            //Toast.makeText(this,"No Navigation",Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@SignInActivity,SampleActivity::class.java))
            finish()
        }
        forgetPasswordTextView.setOnClickListener {
            startActivity(Intent(this@SignInActivity,ForgetPassword::class.java))
        }
        createYourAccountTextView.setOnClickListener {
            startActivity(Intent(this@SignInActivity,Account::class.java))
            finish()
        }
        signInButton.setOnClickListener {
            //Toast.makeText(this,"No Navigation",Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@SignInActivity,SampleActivity::class.java))
            finish()
        }
    }
}