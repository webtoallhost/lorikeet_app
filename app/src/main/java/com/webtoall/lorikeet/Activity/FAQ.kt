package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import com.webtoall.lorikeet.R

class FAQ : AppCompatActivity() {
    var passwordInformationViewViewVisibility=false
    var profileStatusInformationViewViewVisibility=false
    var contactsInformationViewViewVisibility=false
    var languageInformationViewViewVisibility=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_f_a_q)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar!!.customView
        val title = view.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "FAQ"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val passwordTitleImageView = findViewById<ImageView>(R.id.activity_f_a_q_passwordTitle_imageView)
        val passwordMessageLinearLayout = findViewById<LinearLayout>(R.id.activity_f_a_q_passwordMessage_linearLayout)
        val profileStatusTitleImageView = findViewById<ImageView>(R.id.activity_f_a_q_profileStatusTitle_imageView)
        val profileStatusMessageLinearLayout = findViewById<LinearLayout>(R.id.activity_f_a_q_profileStatusMessage_linearLayout)
        val contactsTitleImageView = findViewById<ImageView>(R.id.activity_f_a_q_contactsTitle_imageView)
        val contactsMessageLinearLayout = findViewById<LinearLayout>(R.id.activity_f_a_q_contactsMessage_linearLayout)
        val languageTitleImageView = findViewById<ImageView>(R.id.activity_f_a_q_languageTitle_imageView)
        val languageMessageLinearLayout = findViewById<LinearLayout>(R.id.activity_f_a_q_languageMessage_linearLayout)
        passwordTitleImageView.setOnClickListener {
            if(passwordInformationViewViewVisibility){
                passwordInformationViewViewVisibility=false
                passwordMessageLinearLayout.visibility= View.GONE
                passwordTitleImageView.setImageResource(R.drawable.drop_down_list)
            }
            else{
                passwordInformationViewViewVisibility=true
                passwordMessageLinearLayout.visibility= View.VISIBLE
                passwordTitleImageView.setImageResource(R.drawable.drop_down_up_list)
            }
        }

        profileStatusTitleImageView.setOnClickListener {
            if(profileStatusInformationViewViewVisibility){
                profileStatusInformationViewViewVisibility=false
                profileStatusMessageLinearLayout.visibility= View.GONE
                profileStatusTitleImageView.setImageResource(R.drawable.drop_down_list)
            }
            else{
                profileStatusInformationViewViewVisibility=true
                profileStatusMessageLinearLayout.visibility= View.VISIBLE
                profileStatusTitleImageView.setImageResource(R.drawable.drop_down_up_list)
            }
        }
        contactsTitleImageView.setOnClickListener {
            if(contactsInformationViewViewVisibility){
                contactsInformationViewViewVisibility=false
                contactsMessageLinearLayout.visibility= View.GONE
                contactsTitleImageView.setImageResource(R.drawable.drop_down_list)
            }
            else{
                contactsInformationViewViewVisibility=true
                contactsMessageLinearLayout.visibility= View.VISIBLE
                contactsTitleImageView.setImageResource(R.drawable.drop_down_up_list)
            }
        }
        languageTitleImageView.setOnClickListener {
            if(languageInformationViewViewVisibility){
                languageInformationViewViewVisibility=false
                languageMessageLinearLayout.visibility= View.GONE
                languageTitleImageView.setImageResource(R.drawable.drop_down_list)
            }
            else{
                languageInformationViewViewVisibility=true
                languageMessageLinearLayout.visibility= View.VISIBLE
                languageTitleImageView.setImageResource(R.drawable.drop_down_up_list)
            }
        }
    }
}