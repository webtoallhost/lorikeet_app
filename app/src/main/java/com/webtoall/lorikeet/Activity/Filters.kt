package com.webtoall.lorikeet.Activity


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Adapter.FilterAdapter
import com.webtoall.lorikeet.Adapter.FilterMain
import com.webtoall.lorikeet.Adapter.FiltersMainAdapter
import com.webtoall.lorikeet.Adapter.filter
import com.webtoall.lorikeet.R

class Filters : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filters)
        val filterMainRecyclerView = findViewById<RecyclerView>(R.id.activity_filters_filters_main_item_recyclerView)
        val filterSubRecyclerView = findViewById<RecyclerView>(R.id.activity_filters_filters_sub_item_recyclerView)

        /*val filter = arrayOf<String>(
            "Sort of",
            "Price",
            "New Arrivals",
            "Colors",
            "Size",
            "Brand",
            "Location",
            "Product",
            "Discount",
            "Available",
            "Sales"
        )
        val listView = findViewById<RecyclerView>(R.id.activity_filters_filters_main_item_recyclerView)
        listView.adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, filter)
        listView.setOnItemClickListener { parent, view, position, id ->
            val toast = Toast.makeText(this, "You Clicked" + filter[position], Toast.LENGTH_LONG)
            toast.show()
            toast.setGravity(Gravity.CENTER, 0, 0)
        }*/

        val listItemMain = listOf(
            FilterMain(R.drawable.selected_item, "Sort By"),
            FilterMain(R.drawable.selected_item, "Price"),
            FilterMain(R.drawable.selected_item, "New Arrivals"),
            FilterMain(R.drawable.selected_item, "Colors"),
            FilterMain(R.drawable.selected_item, "Size"),
            FilterMain(R.drawable.selected_item, "Brand"),
            FilterMain(R.drawable.selected_item, "Location"),
            FilterMain(R.drawable.selected_item, "Product"),
            FilterMain(R.drawable.selected_item, "Discount"),
            FilterMain(R.drawable.selected_item, "Available"),
            FilterMain(R.drawable.selected_item, "Sales")
        )
        filterMainRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        filterMainRecyclerView.setHasFixedSize(true)
        filterMainRecyclerView.adapter = FiltersMainAdapter(this, listItemMain)

        val listItem = listOf(
            filter(R.drawable.selected_item, "Relevance"),
            filter(R.drawable.selected_item, "Popularity"),
            filter(R.drawable.selected_item, "Price Low to High"),
            filter(R.drawable.selected_item, "Price High to Low")

        )
        filterSubRecyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        filterSubRecyclerView.setHasFixedSize(true)
        filterSubRecyclerView.adapter = FilterAdapter(this, listItem)
    }
}