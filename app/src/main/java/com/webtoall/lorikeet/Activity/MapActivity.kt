package com.webtoall.lorikeet.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class MapActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        val enterManualButton = findViewById<AppCompatButton>(R.id.activity_map_enter_manual_button)
        val mapContinueButton = findViewById<MaterialButton>(R.id.activity_map_continue_button)
        val mapSkipTextView = findViewById<TextView>(R.id.activity_map_skip_textView)
        val mapBackPressArrowImageView = findViewById<ImageView>(R.id.activity_map_back_press_arrow_imageView)

        mapBackPressArrowImageView.setOnClickListener {
            finish()
        }
        mapSkipTextView.setOnClickListener {
            //Toast.makeText(this,"No Navigation", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@MapActivity,SampleActivity::class.java))
            finish()
        }
        enterManualButton.setOnClickListener {
            startActivity(Intent(this@MapActivity,MapAddDeliveryAddress::class.java))
            //finish()
        }
        mapContinueButton.setOnClickListener {
            //Toast.makeText(this,"No Navigation", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@MapActivity,SampleActivity::class.java))
            finish()
        }
    }
}
