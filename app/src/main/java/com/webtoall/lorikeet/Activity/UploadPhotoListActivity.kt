package com.webtoall.lorikeet.Activity

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R
import java.util.*


class UploadPhotoListActivity : AppCompatActivity() {

    var selectStartingTime: ImageButton? = null
    var selectEndingTime: ImageButton? = null
    var selectDate: ImageButton? = null
    var date: TextView? = null
    var startingTime: TextView? = null
    var endingTime: TextView? = null
    var datePickerDialog: DatePickerDialog? = null
    var timePickerDialog: TimePickerDialog? = null
    var timePickerDialog1: TimePickerDialog? = null

    var h = 0
    var m = 0
    var h1 = 0
    var m1 = 0
    var c: Calendar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_photo_list)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Upload Photo List"
        title.gravity = Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }
        
        val uploadPhotoListCheckOutButton = findViewById<MaterialButton>(R.id.activity_upload_photo_list_checkOut_button)

        uploadPhotoListCheckOutButton.setOnClickListener {
            startActivity(Intent(this,TailorReqProfessional::class.java))
        }

        selectDate = findViewById(R.id.activity_upload_photo_list_date_picker)
        selectStartingTime = findViewById(R.id.activity_upload_photo_list_starting_time_picker)
        selectEndingTime = findViewById(R.id.activity_upload_photo_list_ending_time_picker)
        date = findViewById(R.id.activity_upload_photo_list_date_textView)
        c = Calendar.getInstance()
        val year = c!!.get(Calendar.YEAR)
        val month = c!!.get(Calendar.MONTH)
        val dayOfMonth = c!!.get(Calendar.DAY_OF_MONTH)
        startingTime = findViewById(R.id.activity_upload_photo_list_starting_time_textView)
        endingTime = findViewById(R.id.activity_upload_photo_list_ending_time_textView)
        val monthVal=month+1
        if(monthVal>12) {
            date?.text = "$dayOfMonth/${monthVal-1}/$year"
        }else{
            date?.text = "$dayOfMonth/$monthVal/$year"
        }

        selectDate?.setOnClickListener {

            datePickerDialog = DatePickerDialog(
                    this,
                    DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                        // Display Selected date in TextView
                        val monthVal=month+1
                        if(monthVal>12) {
                        date?.text = "$dayOfMonth/${monthVal-1}/$year"
                    }else{
                        date?.text = "$dayOfMonth/$monthVal/$year"
                    }
                    },
                    year,
                    month,
                    dayOfMonth
            )
            datePickerDialog?.show()


        }

        selectStartingTime?.setOnClickListener {

            timePickerDialog = TimePickerDialog(
                    this,
                    TimePickerDialog.OnTimeSetListener { view, h, m ->
                        val starting_AM_PM = if (h < 12) {
                            "AM"
                        } else {
                            "PM"
                        }

                        if (m == 0) {
                            startingTime?.text = "$h$starting_AM_PM"
                        } else {
                            startingTime?.text = "$h:$m$starting_AM_PM"
                        }
                    },
                    h,
                    m,
                    false
            )
            timePickerDialog?.show()

        }
        selectEndingTime?.setOnClickListener {
            timePickerDialog1 = TimePickerDialog(
                    this,
                    TimePickerDialog.OnTimeSetListener { view, h1, m1 ->
                        val ending_AM_PM: String = if (h1 < 12) {
                            "AM"
                        } else {
                            "PM"
                        }
                        if (m1 == 0) {
                            endingTime?.text = "$h1$ending_AM_PM"
                        } else {
                            endingTime?.text = "$h1:$m1$ending_AM_PM"
                        }
                    },
                    h1,
                    m1,
                    false)
            timePickerDialog1?.show()

        }
    }

}