package com.webtoall.lorikeet.Activity

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Adapter.GridImageAdapter
import com.webtoall.lorikeet.CategoryAdapter
import com.webtoall.lorikeet.DataModel.item
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.category

class ShowroomActivityWithFabrics : AppCompatActivity() {

    private var arrayList: ArrayList<item>?= null
    private var arrayList1: ArrayList<item>?= null

    private var gridImageAdapter : GridImageAdapter?= null
    private var gridImageAdapter1 : GridImageAdapter?= null


    private var hasNextPage: Boolean = true
    val TAG: String = this::class.java.name
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_showroom_fabric)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Showroom"
        title.gravity= Gravity.CENTER

        val gridView = findViewById<RecyclerView>(R.id.showroom_item_gridview)
        val fabricGridView = findViewById<RecyclerView>(R.id.showroom_fabric_gridview)
        arrayList = ArrayList()
        arrayList1 = ArrayList()
        //arrayList = setDataList()

        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )

        gridImageAdapter = GridImageAdapter(applicationContext, arrayList!!)
        gridView?.adapter = gridImageAdapter

        arrayList1?.add(
            item(
                R.drawable.sample3,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList1?.add(
            item(
                R.drawable.sample3,
                "Dress Name Here",
                "$462.94"
            )
        )

        gridImageAdapter1 = GridImageAdapter(applicationContext, arrayList1!!)
        fabricGridView?.adapter = gridImageAdapter1

        val CategoryrV = findViewById<RecyclerView>(R.id.showroom_category_recyclerview)

        val category = listOf(
            category(
                R.drawable.sample2,
                "Kids"
            ),
            category(
                R.drawable.sample2,
                "Men"
            ),
            category(
                R.drawable.sample2,
                "Women"
            ),
            category(
                R.drawable.sample2,
                "Offers"
            ),
            category(
                R.drawable.sample2,
                "New"
            )
        )

        CategoryrV.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL,false)
        CategoryrV.setHasFixedSize(true)
        CategoryrV.adapter= CategoryAdapter(this, category)

    }

}