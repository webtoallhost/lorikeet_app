package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class FinalYourProduct : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_final_your_product)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Tailor Mens Style"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val finalYourProductNextButton = findViewById<MaterialButton>(R.id.activity_final_your_product_next_button)

        finalYourProductNextButton.setOnClickListener {
            startActivity(Intent(this,SelectMeasurement::class.java))
        }
    }
}