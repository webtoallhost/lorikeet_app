package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.webtoall.lorikeet.R


class NeedHelp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_need_help)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Need Help"
        title.gravity= Gravity.CENTER
        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }


        val simpleList = findViewById<ListView>(R.id.simpleListView)

        var needHelpList =
            arrayOf("Report a Problem", "FAQ", "Call Us", "Send us Email")

        val arrayAdapter = ArrayAdapter(
            this,
            R.layout.need_help_list_item,
            R.id.need_help_list_item_textView,
            needHelpList
        )
        simpleList!!.adapter = arrayAdapter

        simpleList.setOnItemClickListener { parent, view, position, id ->
            if(position==0){
                startActivity(Intent(this,ReportProblem::class.java))
            }
            else if(position==1){
                startActivity(Intent(this,FAQ::class.java))
            }
            else if(position==2){
                startActivity(Intent(this,CallUs::class.java))
            }
            else{
                startActivity(Intent(this,SendUsEmail::class.java))
            }
        }
    }
}