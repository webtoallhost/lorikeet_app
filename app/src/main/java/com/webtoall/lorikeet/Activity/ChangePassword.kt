package com.webtoall.lorikeet.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class ChangePassword : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        val changePasswordNextButton = findViewById<MaterialButton>(R.id.activity_change_password_next_button)
        changePasswordNextButton.setOnClickListener {
            startActivity(Intent(this@ChangePassword,SignInActivity::class.java))
        }
    }
}