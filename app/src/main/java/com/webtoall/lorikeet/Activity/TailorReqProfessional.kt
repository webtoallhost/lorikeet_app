package com.webtoall.lorikeet.Activity

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.Adapter.TailorReqProfessionalAdapterColor
import com.webtoall.lorikeet.Adapter.TailorReqProfessionalAdapterFabrics
import com.webtoall.lorikeet.R
import java.util.*

class TailorReqProfessional : AppCompatActivity() {


    private var tailor_req_professional_recycleView_color: RecyclerView?=null
    private var tailor_req_professional_recycleView_fabrics: RecyclerView?=null


    private var imagesListcolor= mutableListOf<Int>()
    private var imagesListfabric= mutableListOf<Int>()

    var selectStartingTime: ImageButton? = null
    var selectEndingTime: ImageButton? = null
    var selectDate: ImageButton? = null
    var date: TextView? = null
    var startingTime: TextView? = null
    var endingTime: TextView? = null
    var datePickerDialog: DatePickerDialog? = null
    var timePickerDialog: TimePickerDialog? = null
    var timePickerDialog1: TimePickerDialog? = null

    var h = 0
    var m = 0
    var h1 = 0
    var m1 = 0
    var c: Calendar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tailor_req_professional)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Upload Photo List"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val tailorReqProfessionalsCheckOutButton = findViewById<MaterialButton>(R.id.activity_tailor_req_professionals_checkOut_button)
        tailorReqProfessionalsCheckOutButton.setOnClickListener {
            startActivity(Intent(this,CustomCart::class.java))
        }
        tailor_req_professional_recycleView_color=findViewById(R.id.tailor_req_professional_recycleView_color)
        tailor_req_professional_recycleView_fabrics=findViewById(R.id.tailor_req_professional_recycleView_fabrics)

        postToList()

        tailor_req_professional_recycleView_color?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        tailor_req_professional_recycleView_color?.setHasFixedSize(true)
        tailor_req_professional_recycleView_color?.adapter=
            TailorReqProfessionalAdapterColor(
                this,
                imagesListcolor
            )


        postToList1()

        tailor_req_professional_recycleView_fabrics?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        tailor_req_professional_recycleView_fabrics?.setHasFixedSize(true)
        tailor_req_professional_recycleView_fabrics?.adapter=
            TailorReqProfessionalAdapterFabrics(
                this,
                imagesListfabric
            )

        selectDate = findViewById(R.id.activity_tailor_req_professional_date_picker)
        selectStartingTime = findViewById(R.id.activity_tailor_req_professional_starting_time_picker)
        selectEndingTime = findViewById(R.id.activity_tailor_req_professional_ending_time_picker)
        date = findViewById(R.id.activity_tailor_req_professional_date_textView)
        c = Calendar.getInstance()
        val year = c!!.get(Calendar.YEAR)
        val month = c!!.get(Calendar.MONTH)
        val dayOfMonth = c!!.get(Calendar.DAY_OF_MONTH)
        startingTime = findViewById(R.id.activity_tailor_req_professional_starting_time_textView)
        endingTime = findViewById(R.id.activity_tailor_req_professional_ending_time_textView)
        val monthVal=month+1
        if(monthVal>12) {
            date?.text = "$dayOfMonth/${monthVal-1}/$year"
        }else{
            date?.text = "$dayOfMonth/$monthVal/$year"
        }

        selectDate?.setOnClickListener {

            datePickerDialog = DatePickerDialog(
                    this,
                    DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                        // Display Selected date in TextView
                        val monthVal=month+1
                        if(monthVal>12) {
                            date?.text = "$dayOfMonth/${monthVal-1}/$year"
                        }else{
                            date?.text = "$dayOfMonth/$monthVal/$year"
                        }
                    },
                    year,
                    month,
                    dayOfMonth
            )
            datePickerDialog?.show()


        }

        selectStartingTime?.setOnClickListener {

            timePickerDialog = TimePickerDialog(
                    this,
                    TimePickerDialog.OnTimeSetListener { view, h, m ->
                        val starting_AM_PM = if (h < 12) {
                            "AM"
                        } else {
                            "PM"
                        }

                        if (m == 0) {
                            startingTime?.text = "$h$starting_AM_PM"
                        } else {
                            startingTime?.text = "$h:$m$starting_AM_PM"
                        }
                    },
                    h,
                    m,
                    false
            )
            timePickerDialog?.show()

        }
        selectEndingTime?.setOnClickListener {
            timePickerDialog1 = TimePickerDialog(
                    this,
                    TimePickerDialog.OnTimeSetListener { view, h1, m1 ->
                        val ending_AM_PM: String = if (h1 < 12) {
                            "AM"
                        } else {
                            "PM"
                        }
                        if (m1 == 0) {
                            endingTime?.text = "$h1$ending_AM_PM"
                        } else {
                            endingTime?.text = "$h1:$m1$ending_AM_PM"
                        }
                    },
                    h1,
                    m1,
                    false)
            timePickerDialog1?.show()

        }


    }
    private fun addToList(image:Int){
        imagesListcolor.add(image)
    }

    private fun postToList() {

        addToList(R.drawable.color_green)
        addToList(R.drawable.color_blue)
        addToList(R.drawable.color_red)
        addToList(R.drawable.color_brown)

    }

    private fun addToList1(image:Int){
        imagesListfabric.add(image)
    }

    private fun postToList1() {

        addToList1(R.drawable.color_green)
        addToList1(R.drawable.color_blue)
        addToList1(R.drawable.color_red)
        addToList1(R.drawable.color_green)

    }



}



