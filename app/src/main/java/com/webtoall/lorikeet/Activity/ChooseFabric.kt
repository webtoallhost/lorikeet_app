package com.webtoall.lorikeet.Activity

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Adapter.*
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.Adapter.SizeAdapter
import com.webtoall.lorikeet.DataModel.item

class ChooseFabric : AppCompatActivity() {


    private var arrival_recyclerView: RecyclerView?=null
    private var products_recycleView_size: RecyclerView?=null
    private var products_recycleView_color: RecyclerView?=null
    private var choosefabric_recycleView_fabric:RecyclerView?=null
    private var products_image_recyclerView:RecyclerView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_fabric)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Sportswear Women"
        title.gravity= Gravity.CENTER

        arrival_recyclerView=findViewById(R.id.arrival_recyclerView)
        products_recycleView_size=findViewById(R.id.products_recycleView_size)
        products_recycleView_color=findViewById(R.id.products_recycleView_color)
        choosefabric_recycleView_fabric=findViewById(R.id.choosefabric_recycleView_fabric)
        products_image_recyclerView=findViewById(R.id.activity_products_image_recyclerView)


        val items = listOf(
            item(
                R.drawable.model1,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model2,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model1,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model2,
                "Dress Name Here",
                "$516.44"
            )
        )

        arrival_recyclerView?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        arrival_recyclerView?.setHasFixedSize(true)
        arrival_recyclerView?.adapter=
            ProductsAdapter(this, items)

        products_image_recyclerView?.adapter= ProductsDetailsImageAdapter(this,items)

        val size = listOf(
            SizeAdapter.size("XS"),
            SizeAdapter.size("M"),
            SizeAdapter.size("L"),
            SizeAdapter.size("XL"),
            SizeAdapter.size("2XL")

        )

        products_recycleView_size?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        products_recycleView_size?.setHasFixedSize(true)
        products_recycleView_size?.adapter= SizeAdapter(this, size)

        val color1=listOf(
            color1(R.drawable.color_background),
            color1(R.drawable.color_background1),
            color1(R.drawable.color_background2),
            color1(R.drawable.color_background3)
        )

        products_recycleView_color?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        products_recycleView_color?.setHasFixedSize(true)
        products_recycleView_color?.adapter= ShapesAdapter(this, color1)


        val color2=listOf(
            color2(R.drawable.color_background),
            color2(R.drawable.color_background1),
            color2(R.drawable.color_background2),
            color2(R.drawable.color_background3)
        )

        choosefabric_recycleView_fabric?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        choosefabric_recycleView_fabric?.setHasFixedSize(true)
        choosefabric_recycleView_fabric?.adapter= ChooseFabricAdapter(this, color2)
    }
}