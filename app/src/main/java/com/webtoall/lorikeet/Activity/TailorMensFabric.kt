package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.Adapter.ShapesAdapter
import com.webtoall.lorikeet.Adapter.TailorMensFabricAdapter
import com.webtoall.lorikeet.Adapter.TailorMensFabricAdapterColor
import com.webtoall.lorikeet.Adapter.color1
import com.webtoall.lorikeet.R

class TailorMensFabric : AppCompatActivity() {

    private var products_recycleView_color: RecyclerView? = null
    private var tailor_mens_fabric_recycleview_solidcolor: RecyclerView? = null
    private var tailor_mens_fabric_recycleview_pattern: RecyclerView? = null

    private var imagesList = mutableListOf<Int>()
    private var imagesList1 = mutableListOf<Int>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tailor_mens_fabric)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Tailor Mens Fabric"
        title.gravity = Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val tailorMensFabricNextButton = findViewById<MaterialButton>(R.id.activity_tailor_mens_fabric_next_button)
        val tailorMensFabricPlusButton = findViewById<ImageView>(R.id.activity_tailor_mens_fabric_plus_button)

        tailorMensFabricPlusButton.setOnClickListener {
            startActivity(Intent(this,UploadPhoto::class.java))
        }

        tailorMensFabricNextButton.setOnClickListener {
            startActivity(Intent(this,SelectStyle::class.java))
        }


        products_recycleView_color = findViewById(R.id.products_recycleView_color)
        tailor_mens_fabric_recycleview_solidcolor = findViewById(R.id.tailor_mens_fabric_recycleview_solidcolor)
        tailor_mens_fabric_recycleview_pattern = findViewById(R.id.tailor_mens_fabric_recycleview_pattern)


        val color1 = listOf(
                color1(R.drawable.color_white),
                color1(R.drawable.color_white),
                color1(R.drawable.color_white)

        )

        products_recycleView_color?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        products_recycleView_color?.setHasFixedSize(true)
        products_recycleView_color?.adapter =
                ShapesAdapter(this, color1)

        postToList()

        tailor_mens_fabric_recycleview_solidcolor?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        tailor_mens_fabric_recycleview_solidcolor?.setHasFixedSize(true)
        tailor_mens_fabric_recycleview_solidcolor?.adapter =
                TailorMensFabricAdapter(
                        this,
                        imagesList
                )


        postToList1()

        tailor_mens_fabric_recycleview_pattern?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        tailor_mens_fabric_recycleview_pattern?.setHasFixedSize(true)
        tailor_mens_fabric_recycleview_pattern?.adapter =
                TailorMensFabricAdapterColor(
                        this,
                        imagesList1
                )


    }

    private fun addToList(image: Int) {
        imagesList.add(image)
    }

    private fun postToList() {

        addToList(R.drawable.color_green)
        addToList(R.drawable.color_blue)
        addToList(R.drawable.color_red)
        addToList(R.drawable.color_brown)
        addToList(R.drawable.color_green)
        addToList(R.drawable.color_blue)
        addToList(R.drawable.color_red)
        addToList(R.drawable.color_brown)
        addToList(R.drawable.color_green)
        addToList(R.drawable.color_blue)
        addToList(R.drawable.color_green)
        addToList(R.drawable.color_blue)
        addToList(R.drawable.color_red)
        addToList(R.drawable.color_brown)
    }

    private fun addToList1(image: Int) {
        imagesList1.add(image)
    }

    private fun postToList1() {

        addToList1(R.drawable.color_green)
        addToList1(R.drawable.color_blue)
        addToList1(R.drawable.color_red)
        addToList1(R.drawable.color_green)

    }


}

