package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Adapter.MyOrdersListAdapter
import com.webtoall.lorikeet.DataModel.MyOrdersDataModel
import com.webtoall.lorikeet.R

class MyOrders : AppCompatActivity() {

    private var arrayList: ArrayList<MyOrdersDataModel>?= null
    private var myOrdersListAdapter : MyOrdersListAdapter?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_orders)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar!!.customView
        val title = view.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "My Orders"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val myOrdersListRecyclerView = findViewById<RecyclerView>(R.id.activity_my_orders_recyclerview)
        arrayList = ArrayList()
        //arrayList = setDataList()

        arrayList?.add(MyOrdersDataModel(R.drawable.sample1,"On Going...","$462.94",1))
        arrayList?.add(MyOrdersDataModel(R.drawable.sample1,"Completed","$462.94",2))
        arrayList?.add(MyOrdersDataModel(R.drawable.sample1,"Cancelled","$462.94",3))

        myOrdersListAdapter = MyOrdersListAdapter(this, arrayList!!)
        myOrdersListRecyclerView?.adapter = myOrdersListAdapter
    }
}