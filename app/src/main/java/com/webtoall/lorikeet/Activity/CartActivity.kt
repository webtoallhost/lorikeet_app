package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.CartAdapter
import com.webtoall.lorikeet.R
import com.webtoall.lorikeet.cart

class CartActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Cart(04)"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val cartSelectAddressButton = findViewById<MaterialButton>(R.id.activity_cart_select_Address_button)
        cartSelectAddressButton.setOnClickListener {
            val intent = Intent(this,ShippingAddress::class.java)
            intent.putExtra("From","Cart")
            startActivity(intent)
        }


        val cartItemRv = findViewById<RecyclerView>(R.id.cart_recyclerView)

        val languages = resources.getStringArray(R.array.Languages)
        /*val spinner = findViewById<Spinner>(R.id.cart_count_spinner)
        if(spinner != null) {
            val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages)
            spinner.adapter = adapter
            spinner.onItemSelectedListener = object :
                    AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>,
                                            view: View, position: Int, id: Long) {

                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }*/

        val cartlist = listOf(
            cart(
                R.drawable.sample2,
                "WROGN Men Blue Slim Fit Mid-Rise Mildy Distressed Strechable Jeans",
                "$516.44",
                "$516.44"
            ),
            cart(
                R.drawable.sample2,
                "WROGN Men Blue Slim Fit Mid-Rise Mildy Distressed Strechable Jeans",
                "$516.44",
                "$516.44"
            ),
            cart(
                R.drawable.sample2,
                "WROGN Men Blue Slim Fit Mid-Rise Mildy Distressed Strechable Jeans",
                "$516.44",
                "$516.44"
            )
            )
        cartItemRv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        cartItemRv.setHasFixedSize(true)
        cartItemRv.adapter= CartAdapter(this, cartlist)
    }
}