package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class ForgetPassword : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)
        val forgetPasswordContinueButton = findViewById<MaterialButton>(R.id.activity_forget_password_continue_button)
        val forgetPasswordBackPressArrowImageView = findViewById<ImageView>(R.id.activity_forget_password_back_press_arrow_imageView)
        forgetPasswordContinueButton.setOnClickListener {
            val intent = Intent(this@ForgetPassword,OTPVerificationActivity::class.java)
            intent.putExtra("From", "ForgetPassword")
            startActivity(intent)
            finish()
        }
        forgetPasswordBackPressArrowImageView.setOnClickListener {
            finish()
        }
    }
}