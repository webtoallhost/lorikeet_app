package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Adapter.GridImageAdapterCollections
import com.webtoall.lorikeet.DataModel.collections
import com.webtoall.lorikeet.R

class TailorCollectionActivity : AppCompatActivity() {

    private var arrayList2: ArrayList<collections>?= null
    private var gridImageAdapter2 : GridImageAdapterCollections?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tailor_collection)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Tailor Mens"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val maleRadioButton = findViewById<ImageView>(R.id.tailorCollection_male_tick)
        val femaleRadioButton = findViewById<ImageView>(R.id.tailorCollection_female_tick)
        val kidsRadioButton = findViewById<ImageView>(R.id.tailorCollection_kids_tick)
        val maleLayout = findViewById<LinearLayout>(R.id.tailorCollection_male_radio_button)
        val femaleLayout = findViewById<LinearLayout>(R.id.tailorCollection_female_radio_button)
        val kidsLayout = findViewById<LinearLayout>(R.id.tailorCollection_kids_radio_button)

        val tailorCollectionPlusImageView = findViewById<ImageView>(R.id.activity_tailor_collection_plus_imageView)

        tailorCollectionPlusImageView.setOnClickListener {
            startActivity(Intent(this, TailorMensFabric::class.java))
        }

        maleLayout.setOnClickListener{
            maleRadioButton.setImageResource(R.drawable.choose_language)
            femaleRadioButton.setImageResource(R.drawable.white_sphere)
            kidsRadioButton.setImageResource(R.drawable.white_sphere)
        }
        femaleLayout.setOnClickListener{
            femaleRadioButton.setImageResource(R.drawable.choose_language)
            maleRadioButton.setImageResource(R.drawable.white_sphere)
            kidsRadioButton.setImageResource(R.drawable.white_sphere)
        }
        kidsLayout.setOnClickListener{
            kidsRadioButton.setImageResource(R.drawable.choose_language)
            femaleRadioButton.setImageResource(R.drawable.white_sphere)
            maleRadioButton.setImageResource(R.drawable.white_sphere)
        }


        val gridView2 = findViewById<RecyclerView>(R.id.tailorCollection_item_rv)
        arrayList2 = ArrayList()

        arrayList2?.add(
            collections(
                R.drawable.sample4,
                "Custiom Shirts"
            )
        )
        arrayList2?.add(
            collections(
                R.drawable.sample4,
                "Custiom Shirts"
            )
        )
        arrayList2?.add(
            collections(
                R.drawable.sample4,
                "Custiom Shirts"
            )
        )
        arrayList2?.add(
            collections(
                R.drawable.sample4,
                "Custiom Shirts"
            )
        )
        arrayList2?.add(
            collections(
                R.drawable.sample4,
                "Custiom Shirts"
            )
        )
        arrayList2?.add(
            collections(
                R.drawable.sample4,
                "Custiom Shirts"
            )
        )

        gridImageAdapter2 = GridImageAdapterCollections(this, arrayList2!!)
        gridView2?.adapter = gridImageAdapter2
    }
}
