package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class Account : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        val backPressArrowImageView = findViewById<ImageView>(R.id.account_back_press_arrow_imageView)
        val accountSigInTextView = findViewById<TextView>(R.id.activity_account_sigIn_textView)
        val accountNextButton=findViewById<MaterialButton>(R.id.activity_account_next_button)
        backPressArrowImageView.setOnClickListener {
            startActivity(Intent(this@Account,SignInActivity::class.java))
            finish()
        }
        accountSigInTextView.setOnClickListener {
           finish()
        }
        accountNextButton.setOnClickListener {
            val intent = Intent(this@Account,OTPVerificationActivity::class.java)
            intent.putExtra("From", "Register")
            startActivity(intent)
            finish()
        }
    }
}