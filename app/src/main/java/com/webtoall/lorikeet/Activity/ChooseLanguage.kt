package com.webtoall.lorikeet.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Telephony
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class ChooseLanguage : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_language)
        val skipTextView = findViewById<AppCompatTextView>(R.id.activity_choose_language_skip_textView)
        val continueButton = findViewById<MaterialButton>(R.id.activity_choose_language_continue_button)
        val registerAsStoreButton=findViewById<MaterialButton>(R.id.activity_choose_language_registerAsStore_button)
        skipTextView.setOnClickListener {
            startActivity(Intent(this@ChooseLanguage,SignInActivity::class.java))
            finish()
        }
        continueButton.setOnClickListener {
            startActivity(Intent(this@ChooseLanguage,SignInActivity::class.java))
            finish()
        }
        registerAsStoreButton.setOnClickListener {
            startActivity(Intent(this@ChooseLanguage,RegisterAsStore::class.java))
        }
    }
}