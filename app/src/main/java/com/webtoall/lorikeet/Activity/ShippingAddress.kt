package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.Adapter.ShippingAddressAdapter
import com.webtoall.lorikeet.R

class ShippingAddress : AppCompatActivity() {


    private var shippingAddress_recycleView:RecyclerView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shipping_address)

        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Address"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val from=intent.getStringExtra("From")
        val shippingAddressProceedToCheckoutButton = findViewById<MaterialButton>(R.id.activity_shipping_address_proceedToCheckout_button)
        if(from=="Cart"){
            shippingAddressProceedToCheckoutButton.visibility=View.VISIBLE
        }
        else{
            shippingAddressProceedToCheckoutButton.visibility=View.GONE
        }

        shippingAddressProceedToCheckoutButton.setOnClickListener {
            startActivity(Intent(this,OrderSummary::class.java))
        }

        shippingAddress_recycleView=findViewById(R.id.shippingAddress_recycleView)

        val address= listOf(
            ShippingAddressAdapter.address(R.drawable.tickright,"Home","Cecillia Chapman 711-2880 Nullat.."),
            ShippingAddressAdapter.address(R.drawable.tickwhite,"Others","Meta Rani Apartment    Samar Paily Rd,SA Block, Samarpally,Krishna"),
            ShippingAddressAdapter.address(R.drawable.tickwhite,"Home","Cecillia Chapman 711-2880 Nulla t.Cecilia      Chapman 711-2880 Nulla t."),
            ShippingAddressAdapter.address(R.drawable.tickwhite,"Others","Cecillia Chapman 711-2880 Nullat..")

        )

        shippingAddress_recycleView?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        shippingAddress_recycleView?.setHasFixedSize(true)
        shippingAddress_recycleView?.adapter=
            ShippingAddressAdapter(this, address)



    }
}