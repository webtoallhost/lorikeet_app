package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.cardview.widget.CardView
import com.webtoall.lorikeet.R

class MyOrdersDetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_orders_details)

        supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar!!.customView
        val title = view.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "FT1726839090"
        title.gravity= Gravity.CENTER

        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val values = intent.getStringExtra("Value")



        val cancelOrderTextView = findViewById<TextView>(R.id.activity_my_orders_details_cancelOrder_textView)
        val cancelOrderSendDetailsLinearLayout = findViewById<LinearLayout>(R.id.activity_my_orders_details_cancelOrderSendDetails_linearLayout)
        val addReviewTextView = findViewById<TextView>(R.id.activity_my_orders_details_addReview_textView)
        val addReviewStar = findViewById<com.iarcuschin.simpleratingbar.SimpleRatingBar>(
            R.id.activity_my_orders_details_addReview_star
        )
        val addReviewLinearLayout = findViewById<LinearLayout>(R.id.activity_my_orders_details_addReview_linearLayout)
        val myOrderDetailsTrackOrderStatusCardView = findViewById<CardView>(R.id.activity_my_order_details_trackOrderStatus_cardView)
        val myOrdersDetailsDownloadInvoiceCardView = findViewById<CardView>(R.id.activity_my_orders_details_downloadInvoice_cardView)
        /*cancelOrderTextView.setOnClickListener {
            cancelOrderSendDetailsLinearLayout.visibility= View.VISIBLE
            cancelOrderTextView.visibility=View.GONE
        }*/

        addReviewTextView.setOnClickListener {
            addReviewStar.visibility=View.VISIBLE
            addReviewLinearLayout.visibility=View.VISIBLE
            addReviewTextView.visibility=View.GONE
        }

        if(values=="1"){
            myOrderDetailsTrackOrderStatusCardView.visibility=View.VISIBLE
        }
        else if(values=="2"){
            myOrdersDetailsDownloadInvoiceCardView.visibility=View.VISIBLE
        }
    }
}