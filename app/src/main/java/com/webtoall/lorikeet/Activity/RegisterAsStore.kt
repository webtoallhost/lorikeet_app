package com.webtoall.lorikeet.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.webtoall.lorikeet.R

class RegisterAsStore : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_as_store)
        val registerAsStoreBackPressArrowImageView = findViewById<ImageView>(R.id.activity_register_as_store_back_press_arrow_imageView)
        registerAsStoreBackPressArrowImageView.setOnClickListener {
            finish()
        }
    }
}