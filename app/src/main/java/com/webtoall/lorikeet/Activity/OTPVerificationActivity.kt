package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class OTPVerificationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_o_t_p_verification)
        val backPressArrowImageView = findViewById<ImageView>(R.id.activity_o_t_p_verification_back_press_arrow_imageView   )
        val oTPVerificationContinueButton = findViewById<MaterialButton>(R.id.activity_o_t_p_verification_continue_button)
        val from = intent.getStringExtra("From")
        backPressArrowImageView.setOnClickListener {
            finish()
        }
        oTPVerificationContinueButton.setOnClickListener {
            if(from=="Register") {
                startActivity(Intent(this@OTPVerificationActivity, AddYourMeasurements::class.java))
                finish()
            }else{
                startActivity(Intent(this@OTPVerificationActivity, ChangePassword::class.java))
            }
        }
    }
}