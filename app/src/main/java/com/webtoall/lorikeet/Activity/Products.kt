package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.Adapter.*
import com.webtoall.lorikeet.DataModel.item
import com.webtoall.lorikeet.R

class Products : AppCompatActivity() {


     private var arrival_recyclerView: RecyclerView?=null
     private var products_recycleView_size:RecyclerView?=null
     private var products_recycleView_color:RecyclerView?=null
    private var products_image_recyclerView:RecyclerView?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Sportswear Women"
        title.gravity= Gravity.CENTER
        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val productsAddToCartButton = findViewById<MaterialButton>(R.id.activity_products_addToCart_button)
        val productsKnowSizeTextView = findViewById<TextView>(R.id.activity_products_know_size_textView)

        productsKnowSizeTextView.setOnClickListener {
            val displayRectangle = Rect()
            val window: Window = this.window
            window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
            val builder: AlertDialog.Builder = AlertDialog.Builder(this, R.style.CustomAlertDialog)
            val viewGroup = findViewById<ViewGroup>(android.R.id.content)
            val dialogView: View = LayoutInflater.from(it.context).inflate(R.layout.activity_size, viewGroup, false)
            val sizeCloseImageView = dialogView.findViewById<ImageView>(R.id.activity_size_close_imageView)
            builder.setView(dialogView)
            val alertDialog: AlertDialog = builder.create()
            alertDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
            sizeCloseImageView.setOnClickListener {
                alertDialog.dismiss()
            }
            alertDialog.show()
        }

        productsAddToCartButton.setOnClickListener {
            startActivity(Intent(this,CartActivity::class.java))
        }


        arrival_recyclerView=findViewById(R.id.arrival_recyclerView)
        products_recycleView_size=findViewById(R.id.products_recycleView_size)
        products_recycleView_color=findViewById(R.id.products_recycleView_color)
        products_image_recyclerView=findViewById(R.id.activity_products_image_recyclerView)

        val items = listOf(
            item(
                R.drawable.model1,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model2,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model1,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.model2,
                "Dress Name Here",
                "$516.44"
            )
        )

        arrival_recyclerView?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        arrival_recyclerView?.setHasFixedSize(true)
        arrival_recyclerView?.adapter= ProductsAdapter(this, items)

        products_image_recyclerView?.adapter= ProductsDetailsImageAdapter(this,items)

        val size = listOf(
            SizeAdapter.size("XS"),
            SizeAdapter.size("M"),
            SizeAdapter.size("L"),
            SizeAdapter.size("XL"),
            SizeAdapter.size("2XL")

        )

        products_recycleView_size?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        products_recycleView_size?.setHasFixedSize(true)
        products_recycleView_size?.adapter=
            SizeAdapter(this, size)

       val color1=listOf(
           color1(R.drawable.color_background),
           color1(R.drawable.color_background1),
           color1(R.drawable.color_background2),
           color1(R.drawable.color_background3)
       )

        products_recycleView_color?.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        products_recycleView_color?.setHasFixedSize(true)
        products_recycleView_color?.adapter=
            ShapesAdapter(this, color1)









    }
}