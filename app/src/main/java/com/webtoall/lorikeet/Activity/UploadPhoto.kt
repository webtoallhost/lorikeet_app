package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.Adapter.UploadPhotoAdapter
import com.webtoall.lorikeet.Adapter.uploadPhoto
import com.webtoall.lorikeet.R

class UploadPhoto : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_photo)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title =
            view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "Upload Photo List"
        title.gravity = Gravity.CENTER
        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }

        val uploadPhotoRequestProfessionalsButton = findViewById<MaterialButton>(R.id.activity_upload_photo_requestProfessionals_button)
        val uploadPhotoAddMeasurementsButton = findViewById<AppCompatButton>(R.id.activity_upload_photo_addMeasurements_button)

        uploadPhotoRequestProfessionalsButton.setOnClickListener {
           startActivity(Intent(this,UploadPhotoListActivity::class.java))
        }

        uploadPhotoAddMeasurementsButton.setOnClickListener {
            Toast.makeText(this,"No Navigation",Toast.LENGTH_SHORT).show()
        }



        val removerv = findViewById<RecyclerView>(R.id.recycler_upload)

        val pics = listOf(
            uploadPhoto(R.drawable.sample2),
            uploadPhoto(R.drawable.sample2),
            uploadPhoto(R.drawable.sample2),
            uploadPhoto(R.drawable.sample2))
        removerv.setHasFixedSize(true)
        removerv.adapter= UploadPhotoAdapter(this, pics)
    }
}