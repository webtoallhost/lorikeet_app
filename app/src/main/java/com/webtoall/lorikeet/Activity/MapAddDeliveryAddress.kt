package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.button.MaterialButton
import com.webtoall.lorikeet.R

class MapAddDeliveryAddress : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map_add_delivery_address)
        val mapAddDeliveryAddressSkipTextView = findViewById<TextView>(R.id.activity_map_add_delivery_address_skip_textView)
        val mapAddDeliveryAddressSubmitButton = findViewById<TextView>(R.id.activity_map_add_delivery_address_submit_button)
        val mapAddDeliveryAddressBackPressArrowImageView = findViewById<ImageView>(R.id.activity_map_add_delivery_address_back_press_arrow_imageView)
        mapAddDeliveryAddressBackPressArrowImageView.setOnClickListener { finish() }
        mapAddDeliveryAddressSkipTextView.setOnClickListener {
            //Toast.makeText(this,"No Navigation", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@MapAddDeliveryAddress,SampleActivity::class.java))
            finish()
        }
        mapAddDeliveryAddressSubmitButton.setOnClickListener {
            //Toast.makeText(this,"No Navigation", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this@MapAddDeliveryAddress,SampleActivity::class.java))
            finish()
        }
    }
}