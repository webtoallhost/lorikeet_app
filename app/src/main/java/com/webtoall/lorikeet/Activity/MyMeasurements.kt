package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputLayout
import com.webtoall.lorikeet.R

class MyMeasurements : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_measurements)
        supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setCustomView(R.layout.custom_action_bar_with_back_press_button_layout)
        val view = supportActionBar?.customView
        val title = view?.findViewById<View>(R.id.custom_action_bar_with_back_press_button_layout_title_textView) as TextView
        title.text = "My Measurements"
        title.gravity= Gravity.CENTER
        val actionBarBack = view.findViewById<ImageView>(R.id.action_bar_back)
        actionBarBack.setOnClickListener {
            finish()
        }

        val notificationButton = view.findViewById<View>(R.id.action_bar_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_cart) as ImageButton
        notificationButton.setOnClickListener {
            startActivity(Intent(this, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            startActivity(Intent(this, CartActivity::class.java))
        }


        val collarSizeTxtInLayout = findViewById<TextInputLayout>(R.id.activity_my_measurements_collar_size_txtInLayout)
        val shoulderSizeTxtInLayout = findViewById<TextInputLayout>(R.id.activity_my_measurements_shoulder_size_txtInLayout)
        val selectSizeSpinner=findViewById<Spinner>(R.id.activity_my_measurements_select_unit_spinner)
        val selectUnitImageView=findViewById<ImageView>(R.id.activity_my_measurements_select_unit_imageView)
        val addYourMeasurementsNextButton = findViewById<MaterialButton>(R.id.activity_my_measurements_next_button)


        val maleLinearLayout=findViewById<LinearLayout>(R.id.activity_my_measurements_male_linearLayout)
        val feMaleLinearLayout=findViewById<LinearLayout>(R.id.activity_my_measurements_female_linearLayout)
        val maleIconImageView=findViewById<ImageView>(R.id.activity_my_measurements_male_icon_imageView)
        val maleIconTextView=findViewById<TextView>(R.id.activity_my_measurements_male_icon_textView)
        val feMaleIconImageView=findViewById<ImageView>(R.id.activity_my_measurements_female_icon_imageView)
        val feMaleIconTextView=findViewById<TextView>(R.id.activity_my_measurements_female_icon_textView)
        val bodyImageView=findViewById<ImageView>(R.id.activity_my_measurements_body_imageView)
        val adapter: ArrayAdapter<*> = ArrayAdapter.createFromResource(
                this,
                R.array.collar_array,
                R.layout.color_spinner_layout
        )
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_layout)
        selectSizeSpinner.adapter = adapter
        selectSizeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                shoulderSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
                collarSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        selectUnitImageView.setOnClickListener {
            selectSizeSpinner.performClick()
        }

        maleLinearLayout.setOnClickListener {
            maleIconImageView.setImageResource(R.drawable.ic_male_selected)
            maleIconTextView.setTextColor(ContextCompat.getColor(this,R.color.blue))
            feMaleIconImageView.setImageResource(R.drawable.ic_female_unselected)
            feMaleIconTextView.setTextColor(ContextCompat.getColor(this,R.color.white))
            bodyImageView.setImageResource(R.drawable.male_body)
        }

        feMaleLinearLayout.setOnClickListener {
            feMaleIconImageView.setImageResource(R.drawable.ic_female_selected)
            feMaleIconTextView.setTextColor(ContextCompat.getColor(this,R.color.blue))
            maleIconImageView.setImageResource(R.drawable.ic_male_unselected)
            maleIconTextView.setTextColor(ContextCompat.getColor(this,R.color.white))
            bodyImageView.setImageResource(R.drawable.female_body)
        }

        addYourMeasurementsNextButton.setOnClickListener {
           /* startActivity(Intent(this@MyMeasurements,MapActivity::class.java))
            finish()*/
            Toast.makeText(this,"No Navigation",Toast.LENGTH_SHORT).show()
        }
    }
}