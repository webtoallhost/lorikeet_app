package com.webtoall.lorikeet.Activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.google.android.material.textfield.TextInputLayout
import com.webtoall.lorikeet.R


class AddYourMeasurements : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_your_measurements)
        val collarSizeTxtInLayout = findViewById<TextInputLayout>(R.id.activity_add_your_measurements_collar_size_txtInLayout)
        val shoulderSizeTxtInLayout = findViewById<TextInputLayout>(R.id.activity_add_your_measurements_shoulder_size_txtInLayout)
        val selectSizeSpinner=findViewById<Spinner>(R.id.activity_add_your_measurements_select_unit_spinner)
        val selectUnitImageView=findViewById<ImageView>(R.id.activity_add_your_measurements_select_unit_imageView)
        val addYourMeasurementsNextButton = findViewById<MaterialButton>(R.id.activity_add_your_measurements_next_button)
        val addYourMeasurementsSkipTextView = findViewById<TextView>(R.id.activity_add_your_measurements_skip_textView)


        val maleLinearLayout=findViewById<LinearLayout>(R.id.activity_add_your_measurements_male_linearLayout)
        val feMaleLinearLayout=findViewById<LinearLayout>(R.id.activity_add_your_measurements_female_linearLayout)
        val maleIconImageView=findViewById<ImageView>(R.id.activity_add_your_measurements_male_icon_imageView)
        val maleIconTextView=findViewById<TextView>(R.id.activity_add_your_measurements_male_icon_textView)
        val feMaleIconImageView=findViewById<ImageView>(R.id.activity_add_your_measurements_female_icon_imageView)
        val feMaleIconTextView=findViewById<TextView>(R.id.activity_add_your_measurements_female_icon_textView)
        val bodyImageView=findViewById<ImageView>(R.id.activity_add_your_measurements_body_imageView)
        val adapter: ArrayAdapter<*> = ArrayAdapter.createFromResource(
                this,
                R.array.collar_array,
                R.layout.color_spinner_layout
        )
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_layout)
        selectSizeSpinner.adapter = adapter
        selectSizeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                shoulderSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
                collarSizeTxtInLayout.suffixText=parent.getItemAtPosition(position).toString()
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        selectUnitImageView.setOnClickListener {
            selectSizeSpinner.performClick()
        }

        maleLinearLayout.setOnClickListener {
            maleIconImageView.setImageResource(R.drawable.ic_male_selected)
            maleIconTextView.setTextColor(ContextCompat.getColor(this,R.color.blue))
            feMaleIconImageView.setImageResource(R.drawable.ic_female_unselected)
            feMaleIconTextView.setTextColor(ContextCompat.getColor(this,R.color.white))
            bodyImageView.setImageResource(R.drawable.male_body)
        }

        feMaleLinearLayout.setOnClickListener {
            feMaleIconImageView.setImageResource(R.drawable.ic_female_selected)
            feMaleIconTextView.setTextColor(ContextCompat.getColor(this,R.color.blue))
            maleIconImageView.setImageResource(R.drawable.ic_male_unselected)
            maleIconTextView.setTextColor(ContextCompat.getColor(this,R.color.white))
            bodyImageView.setImageResource(R.drawable.female_body)
        }

        addYourMeasurementsNextButton.setOnClickListener {
            startActivity(Intent(this@AddYourMeasurements,MapActivity::class.java))
            finish()
        }
        addYourMeasurementsSkipTextView.setOnClickListener {
            startActivity(Intent(this@AddYourMeasurements,MapActivity::class.java))
            //finish()
        }
    }
}