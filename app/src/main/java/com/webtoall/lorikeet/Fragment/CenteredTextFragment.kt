package com.webtoall.lorikeet.Fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.webtoall.lorikeet.R


class CenteredTextFragment : Fragment() {
    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        @Nullable container: ViewGroup?,
        @Nullable savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_text, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        val args: Bundle? = arguments
        val text =
            if (args != null) args.getString(EXTRA_TEXT) else ""
        val textView: TextView = view.findViewById(R.id.text)
        textView.text = text
        textView.setOnClickListener { v -> Toast.makeText(v.context, text, Toast.LENGTH_SHORT).show() }
    }

    companion object {
        private const val EXTRA_TEXT = "text"
        fun createFor(text: String?): CenteredTextFragment {
            val fragment = CenteredTextFragment()
            val args = Bundle()
            args.putString(EXTRA_TEXT, text)
            fragment.arguments = args
            return fragment
        }
    }
}