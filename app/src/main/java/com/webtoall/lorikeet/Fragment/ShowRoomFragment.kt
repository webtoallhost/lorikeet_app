package com.webtoall.lorikeet.Fragment

import alirezat775.lib.carouselview.Carousel
import alirezat775.lib.carouselview.CarouselLazyLoadListener
import alirezat775.lib.carouselview.CarouselListener
import alirezat775.lib.carouselview.CarouselView
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.*
import com.webtoall.lorikeet.Activity.CartActivity
import com.webtoall.lorikeet.Activity.Notifications
import com.webtoall.lorikeet.Activity.SampleActivity
import com.webtoall.lorikeet.Adapter.*
import com.webtoall.lorikeet.DataModel.item
import com.webtoall.lorikeet.Model.CarouselListModel
import com.webtoall.lorikeet.category

class ShowRoomFragment: Fragment() {
    private var arrayList: ArrayList<item>?= null
    private var gridImageAdapter : GridImageAdapter?= null

    private var hasNextPage: Boolean = true
    val TAG: String = this::class.java.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /* arguments?.let {
             param1 = it.getString(ARG_PARAM1)
             param2 = it.getString(ARG_PARAM2)
         }*/
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.activity_showroom, container, false)

        (activity as AppCompatActivity).supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        (activity as AppCompatActivity).supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setCustomView(R.layout.custom_action_bar_layout)
        val view = (activity as AppCompatActivity).supportActionBar?.customView
        val title =
            view?.findViewById<View>(R.id.custom_action_bar_layout_title_textView) as TextView
        title.text = "Showroom"
        title.gravity= Gravity.CENTER

        val menuButton = view?.findViewById<View>(R.id.action_bar_main_menu) as ImageButton
        val notificationButton = view.findViewById<View>(R.id.action_bar_main_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_main_cart) as ImageButton

        menuButton.setOnClickListener {
            (activity as SampleActivity).drawerLayout!!.openDrawer(GravityCompat.START)
        }

        notificationButton.setOnClickListener {
            context?.startActivity(Intent(context, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            context?.startActivity(Intent(context, CartActivity::class.java))
        }

        val adapter = CouponCarouselListAdapter()
        val carousel = Carousel(activity as AppCompatActivity, rootView.findViewById(R.id.activity_showroom_carousel_view), adapter)
        carousel.setOrientation(CarouselView.HORIZONTAL, false)
        carousel.autoScroll(false, 5000, true)
        carousel.scaleView(true)
        carousel.lazyLoad(true, object : CarouselLazyLoadListener {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: CarouselView) {
                if (hasNextPage) {
                    Log.d(TAG, "load new item on lazy mode")
                    carousel.add(CarouselListModel(11))
                    carousel.add(CarouselListModel(12))
                    carousel.add(CarouselListModel(13))
                    carousel.add(CarouselListModel(14))
                    carousel.add(CarouselListModel(15))
                    hasNextPage = false
                }
            }
        })
        adapter.setOnClickListener(object :
            CouponCarouselListAdapter.OnClick {
            override fun click(model: CarouselListModel) {
                carousel.remove(model)
            }
        })
//        carousel.scrollSpeed(100f)
//        carousel.enableSlider(true)

        carousel.addCarouselListener(object : CarouselListener {
            override fun onPositionChange(position: Int) {
                Log.d(TAG, "currentPosition : $position")
            }

            override fun onScroll(dx: Int, dy: Int) {
                Log.d(TAG, "onScroll dx : $dx -- dy : $dx")
            }
        })

//        carousel.add(EmptySampleModel("empty list"))
        carousel.add(CarouselListModel(1))
        carousel.add(CarouselListModel(2))
        carousel.add(CarouselListModel(3))
        carousel.add(CarouselListModel(4))
        carousel.add(CarouselListModel(5))
        carousel.add(CarouselListModel(6))
        carousel.add(CarouselListModel(7))
        carousel.add(CarouselListModel(8))
        carousel.add(CarouselListModel(9))
        carousel.add(CarouselListModel(10))

        val gridView = rootView.findViewById<RecyclerView>(R.id.showroom_item_gridview)
        arrayList = ArrayList()
        //arrayList = setDataList()

        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )
        arrayList?.add(
            item(
                R.drawable.sample1,
                "Dress Name Here",
                "$462.94"
            )
        )

        gridImageAdapter = GridImageAdapter(context!!, arrayList!!)
        gridView?.adapter = gridImageAdapter

        val CategoryrV = rootView.findViewById<RecyclerView>(R.id.showroom_category_recyclerview)
        val storesItemRv = rootView.findViewById<RecyclerView>(R.id.showroom_stores_item_recyclerView)

        val category = listOf(
            category(
                R.drawable.sample2,
                "Kids"
            ),
            category(
                R.drawable.sample2,
                "Men"
            ),
            category(
                R.drawable.sample2,
                "Women"
            ),
            category(
                R.drawable.sample2,
                "Offers"
            ),
            category(
                R.drawable.sample2,
                "New"
            )
        )

        val items = listOf(
            item(
                R.drawable.sample2,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.sample2,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.sample2,
                "Dress Name Here",
                "$516.44"
            ),
            item(
                R.drawable.sample2,
                "Dress Name Here",
                "$516.44"
            )
        )

        storesItemRv.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        storesItemRv.setHasFixedSize(true)
        storesItemRv.adapter= StoresItemAdapter(context!!, items)

        CategoryrV.layoutManager = LinearLayoutManager(context,RecyclerView.HORIZONTAL,false)
        CategoryrV.setHasFixedSize(true)
        CategoryrV.adapter= CategoryAdapter(context!!, category)

        val showRoomFilterImageView = rootView.findViewById<ImageView>(R.id.activity_showroom_filter_imageView)
        showRoomFilterImageView.setOnClickListener {
            callFilter(context!!)
        }
        return rootView
    }

    private fun callFilter(context: Context) {
        val displayRectangle = Rect()
        val window: Window = (activity as AppCompatActivity).window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        val builder: AlertDialog.Builder = AlertDialog.Builder((activity as AppCompatActivity), R.style.CustomAlertDialog)
        val viewGroup = (activity as AppCompatActivity).findViewById<ViewGroup>(android.R.id.content)
        val dialogView: View = LayoutInflater.from(context).inflate(R.layout.activity_filters, viewGroup, false)

        val filterMainRecyclerView = dialogView.findViewById<RecyclerView>(R.id.activity_filters_filters_main_item_recyclerView)
        val filterSubRecyclerView = dialogView.findViewById<RecyclerView>(R.id.activity_filters_filters_sub_item_recyclerView)
        val filtersCloseImageView = dialogView.findViewById<ImageView>(R.id.activity_filters_close_imageView)
        val listItemMain = listOf(
            FilterMain(R.drawable.selected_item, "Sort By"),
            FilterMain(R.drawable.selected_item, "Price"),
            FilterMain(R.drawable.selected_item, "New Arrivals"),
            FilterMain(R.drawable.selected_item, "Colors"),
            FilterMain(R.drawable.selected_item, "Size"),
            FilterMain(R.drawable.selected_item, "Brand"),
            FilterMain(R.drawable.selected_item, "Location"),
            FilterMain(R.drawable.selected_item, "Product"),
            FilterMain(R.drawable.selected_item, "Discount"),
            FilterMain(R.drawable.selected_item, "Available"),
            FilterMain(R.drawable.selected_item, "Sales")
        )
        filterMainRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        filterMainRecyclerView.setHasFixedSize(true)
        filterMainRecyclerView.adapter = FiltersMainAdapter(context, listItemMain)

        val listItem = listOf(
            filter(R.drawable.selected_item, "Relevance"),
            filter(R.drawable.selected_item, "Popularity"),
            filter(R.drawable.selected_item, "Price Low to High"),
            filter(R.drawable.selected_item, "Price High to Low")

        )
        filterSubRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        filterSubRecyclerView.setHasFixedSize(true)
        filterSubRecyclerView.adapter = FilterAdapter(context, listItem)

        builder.setView(dialogView)
        val alertDialog: AlertDialog = builder.create()
        alertDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        filtersCloseImageView.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    /*  companion object {
          @JvmStatic
          fun newInstance(param1: String, param2: String) =
              FirstFragment().apply {
                  arguments = Bundle().apply {
                      putString(ARG_PARAM1, param1)
                      putString(ARG_PARAM2, param2)
                  }
              }
      }*/
}