package com.webtoall.lorikeet.Fragment

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.webtoall.lorikeet.Activity.*
import com.webtoall.lorikeet.R

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class MyProfileFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       /* arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }*/
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_my_profile, container, false)
        (activity as AppCompatActivity).supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        (activity as AppCompatActivity).supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setCustomView(R.layout.custom_action_bar_layout)
        val view = (activity as AppCompatActivity).supportActionBar?.customView
        val title =
            view?.findViewById<View>(R.id.custom_action_bar_layout_title_textView) as TextView
        title.text = "My Profile"
        title.gravity= Gravity.CENTER

        val menuButton = view?.findViewById<View>(R.id.action_bar_main_menu) as ImageButton
        val notificationButton = view.findViewById<View>(R.id.action_bar_main_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_main_cart) as ImageButton

        menuButton.setOnClickListener {
            (activity as SampleActivity).drawerLayout!!.openDrawer(GravityCompat.START)
        }

        notificationButton.setOnClickListener {
            context?.startActivity(Intent(context, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            context?.startActivity(Intent(context, CartActivity::class.java))
        }

        val myProfileEditProfileCardView = rootView.findViewById<LinearLayout>(R.id.fragment_my_profile_editProfile_linearLayout)
        val myProfileMyAddressesCardView = rootView.findViewById<LinearLayout>(R.id.fragment_my_profile_myAddresses_linearLayout)
        val myProfileMyMeasurementsCardView = rootView.findViewById<LinearLayout>(R.id.fragment_my_profile_myMeasurements_linearLayout)
        myProfileEditProfileCardView.setOnClickListener {
            context?.startActivity(Intent(context, EditProfile::class.java))
        }

        myProfileMyAddressesCardView.setOnClickListener {
            val intent = Intent(context, ShippingAddress::class.java)
            intent.putExtra("From","MyProfile")
            context?.startActivity(intent)
        }

        myProfileMyMeasurementsCardView.setOnClickListener {
            context?.startActivity(Intent(context, MyMeasurements::class.java))
        }

        return rootView
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MyProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}