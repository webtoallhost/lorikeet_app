package com.webtoall.lorikeet.Fragment

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.webtoall.lorikeet.Activity.CartActivity
import com.webtoall.lorikeet.Activity.Notifications
import com.webtoall.lorikeet.Activity.SampleActivity
import com.webtoall.lorikeet.Adapter.*
import com.webtoall.lorikeet.DataModel.item
import com.webtoall.lorikeet.R

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class FavouritesFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null

    private var arrayList: ArrayList<item>?= null
    private var gridImageAdapter : FavouritesGridImageAdapter?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_favourites, container, false)
        (activity as AppCompatActivity).supportActionBar?.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        (activity as AppCompatActivity).supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2879FE")))
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowCustomEnabled(true)
        (activity as AppCompatActivity).supportActionBar?.setCustomView(R.layout.custom_action_bar_layout)
        val view = (activity as AppCompatActivity).supportActionBar?.customView
        val title =
            view?.findViewById<View>(R.id.custom_action_bar_layout_title_textView) as TextView
        title.text = "Favourites"
        title.gravity= Gravity.CENTER

        val menuButton = view?.findViewById<View>(R.id.action_bar_main_menu) as ImageButton
        val notificationButton = view.findViewById<View>(R.id.action_bar_main_notification) as ImageButton
        val cartButton = view.findViewById<View>(R.id.action_bar_main_cart) as ImageButton

        menuButton.setOnClickListener {
            (activity as SampleActivity).drawerLayout!!.openDrawer(GravityCompat.START)
        }

        notificationButton.setOnClickListener {
            context?.startActivity(Intent(context, Notifications::class.java))
        }

        cartButton.setOnClickListener {
            context?.startActivity(Intent(context, CartActivity::class.java))
        }

        val gridView = rootView.findViewById<RecyclerView>(R.id.activity_favourites_item_gridview)
        arrayList = ArrayList()
        //arrayList = setDataList()

        arrayList?.add(
                item(
                        R.drawable.sample1,
                        "Dress Name Here",
                        "$462.94"
                )
        )
        arrayList?.add(
                item(
                        R.drawable.sample1,
                        "Dress Name Here",
                        "$462.94"
                )
        )
        arrayList?.add(
                item(
                        R.drawable.sample1,
                        "Dress Name Here",
                        "$462.94"
                )
        )
        arrayList?.add(
                item(
                        R.drawable.sample1,
                        "Dress Name Here",
                        "$462.94"
                )
        )

        gridImageAdapter = FavouritesGridImageAdapter(context!!, arrayList!!)
        gridView?.adapter = gridImageAdapter

        val favouritesFilterImageView = rootView.findViewById<ImageView>(R.id.fragment_favourites_filter_imageView)
        favouritesFilterImageView.setOnClickListener {
            callFilter(context!!)
        }
        return rootView
    }

    private fun callFilter(context: Context) {
        val displayRectangle = Rect()
        val window: Window = (activity as AppCompatActivity).window
        window.decorView.getWindowVisibleDisplayFrame(displayRectangle)
        val builder: AlertDialog.Builder = AlertDialog.Builder((activity as AppCompatActivity), R.style.CustomAlertDialog)
        val viewGroup = (activity as AppCompatActivity).findViewById<ViewGroup>(android.R.id.content)
        val dialogView: View = LayoutInflater.from(context).inflate(R.layout.activity_filters, viewGroup, false)

        val filterMainRecyclerView = dialogView.findViewById<RecyclerView>(R.id.activity_filters_filters_main_item_recyclerView)
        val filterSubRecyclerView = dialogView.findViewById<RecyclerView>(R.id.activity_filters_filters_sub_item_recyclerView)
        val filtersCloseImageView = dialogView.findViewById<ImageView>(R.id.activity_filters_close_imageView)
        val listItemMain = listOf(
            FilterMain(R.drawable.selected_item, "Sort By"),
            FilterMain(R.drawable.selected_item, "Price"),
            FilterMain(R.drawable.selected_item, "New Arrivals"),
            FilterMain(R.drawable.selected_item, "Colors"),
            FilterMain(R.drawable.selected_item, "Size"),
            FilterMain(R.drawable.selected_item, "Brand"),
            FilterMain(R.drawable.selected_item, "Location"),
            FilterMain(R.drawable.selected_item, "Product"),
            FilterMain(R.drawable.selected_item, "Discount"),
            FilterMain(R.drawable.selected_item, "Available"),
            FilterMain(R.drawable.selected_item, "Sales")
        )
        filterMainRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        filterMainRecyclerView.setHasFixedSize(true)
        filterMainRecyclerView.adapter = FiltersMainAdapter(context, listItemMain)

        val listItem = listOf(
            filter(R.drawable.selected_item, "Relevance"),
            filter(R.drawable.selected_item, "Popularity"),
            filter(R.drawable.selected_item, "Price Low to High"),
            filter(R.drawable.selected_item, "Price High to Low")

        )
        filterSubRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        filterSubRecyclerView.setHasFixedSize(true)
        filterSubRecyclerView.adapter = FilterAdapter(context, listItem)

        builder.setView(dialogView)
        val alertDialog: AlertDialog = builder.create()
        alertDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        filtersCloseImageView.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FavouritesFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}